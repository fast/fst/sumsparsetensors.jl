
using Semirings
using SumSparseTensors
using Test

const SST = SumSparseTensors

@testset failfast=true "CustomUnitRange" begin
    r = SST.CustomUnitRange(-2, 3)
    @test length(r) == 6
    @test all(collect(r) .== [-2, -1, 0, 1, 2, 3])
end

@testset verbose=true failfast=true "SumSparseTensor" begin
    @testset "vector" begin
        @test_throws ArgumentError sparsesum([1, 2, 3, 4], ones(3))

        Σx = sparsesum([1, 2, 3, 2], ones(4))
        @test nnz(Σx) == 4
        @test size(Σx) == (3,)
        @test size(Σx, 1) == 3

        Σx = sparsesum([1, 2, 3, 2], ones(4); axis = -1:10)
        @test nnz(Σx) == 4
        @test size(Σx) == (12,)
        @test axes(Σx) == (-1:10,)
        @test size(Σx, 1) == 12

        Σx = sparsesum([1, 2, 3, 2], ones(Bool, 4))
        @test eltype(Σx) <: Bool
        Σx = sparsesum([1, 2, 3, 2], ones(Float32, 4))
        @test eltype(Σx) <: Float32
    end

    @testset "matrix" begin
        Σx = sparsesum([(2, 2), (3, 4), (2, 1)], ones(3))
        @test nnz(Σx) == 3
        @test size(Σx) == (3, 4)
        @test size(Σx, 1) == 3
        @test size(Σx, 2) == 4

        Σx = sparsesum([(2, 2), (3, 4), (4, 1)], ones(3); axes = (1:30, 1:10))
        @test nnz(Σx) == 3
        @test size(Σx) == (30, 10)
        @test size(Σx, 1) == 30
        @test size(Σx, 2) == 10
    end

    @testset "sort" begin
        Σx = sparsesum([(-1, 2), (3, 4), (4, 1)], ones(3); axes = (-1:4, 1:4))
        nzcoo, _ = findnz(sort(Σx, 1))
        @test all(nzcoo .== [(-1, 2), (3, 4), (4, 1)])

        nzcoo, nzval = findnz(sort(Σx, 2))
        @test all(nzcoo .== [(4, 1), (-1, 2), (3, 4)])
    end
end

@testset verbose=true failfast=true "base" begin

    S = LogSemiring{Float64,1}

    @testset "addaxes" begin
        Σx = sparsesum([1, 2, 3, 2], ones(4))
        @test size(addaxes(Σx, 3)) == (3, 1, 1, 1)

        Σx = sparsesum([(2, 2), (3, 4), (2, 1)], ones(3))
        @test size(addaxes(Σx, 2)) == (3, 4, 1, 1)

        sΣx = addaxes(sort(Σx, 2), 2)
        @test sΣx isa AbstractSortedSumSparseTensor
        @test size(sΣx) == (3, 4, 1, 1)
    end

    @testset "cat" begin
        Σx = sparsesum([(1, 2, 3, 4), (4, 3, 2, 1),], ones(S, 2))
        Σy = sparsesum([(1, 2, 2, 4)], ones(S, 1); axes = axes(Σx))
        Σz = cat(Σx, Σy, dims = (2, 3))
        @test size(Σz) == (4, 6, 6, 4)
        @test nnz(Σz) ==  3

        Σx = sparsesum([(1, 2, 3, 4), (4, 3, 2, 1),], ones(S, 2); axes = (1:4, 1:4, 1:4, 0:4))
        Σy = sparsesum([(1, 2, 2, 4)], ones(S, 1); axes = (1:4, 1:4, 1:4, 1:4))
        @test_throws DimensionMismatch cat(Σx, Σy, dims = (2, 3))

        Σx = sparsesum([(1, 2, 3, 4), (4, 3, 2, 1),], ones(S, 2))
        Σy = sparsesum([(1, 2, 2, 4)], ones(S, 1); axes = axes(Σx))
        sΣx = sort(Σx, 3)
        sΣy = sort(Σy, 3)
        sΣz = cat(sΣx, sΣy, dims = (2, 3))
        @test size(sΣz) == (4, 6, 6, 4)
        @test nnz(sΣz) == 3
        @test sΣz isa AbstractSortedSumSparseTensor

        Σx = sparsesum([1, 2, 3, 2], ones(S, 4); axis = 1:10)
        Σy = sparsesum([2, 2], ones(S, 2); axis = -1:7)
        Σz = cat(Σx, Σy)
        @test size(Σz, 1) == size(Σx, 1) + size(Σy, 1)
        @test axes(Σz, 1) == 1:19
        @test nnz(Σz) == 6
    end

    @testset "collapse" begin
        Σx = sparsesum([1, 2, 3, 2], ones(S, 4))
        Σy = collapse(Σx)
        @test size(Σy) == size(Σx)
        @test nnz(Σy) == 3

        xnzcoo, _ = findnz(Σx)
        ynzcoo, _ = findnz(Σy)
        @test Set(xnzcoo) == Set(ynzcoo)

        Σx = sparsesum([(2, 1), (2, 1), (2, 3)], ones(S, 3))
        Σy = collapse(Σx)
        @test size(Σy) == size(Σx)
        @test nnz(Σy) == 2

        xnzcoo, _ = findnz(Σx)
        ynzcoo, _ = findnz(Σy)
        @test Set(xnzcoo) == Set(ynzcoo)
    end

    @testset "copy" begin
        Σx = sparsesum([(2, -2), (3, 4), (2, 1)], ones(3))
        Σy = copy(Σx)
        xcoo, _ = findnz(Σx)
        ycoo, _ = findnz(Σy)
        @test Set(xcoo) == Set(ycoo)

        sΣx = sort(Σx, 2)
        sΣy = copy(sΣx)
        xcoo, _ = findnz(Σx)
        @test Set(xcoo) == Set(ycoo)
        @test sΣy isa AbstractSortedSumSparseTensor
    end

    @testset "insertaxes" begin
        Σx = sparsesum([1, 2, 3, 2], ones(4))
        @test size(insertaxes(Σx, 1 => 1:1, 2 => 1:1)) == (1, 3, 1)

        Σx = sparsesum([(2, 2), (3, 4), (2, 1)], ones(3))
        @test size(insertaxes(Σx, 2 => 1:1, 3 => 1:1)) == (3, 1, 4, 1)

        sΣx = insertaxes(sort(Σx, 2), 2 => 1:1, 3 => 1:1)
        @test sΣx isa AbstractSortedSumSparseTensor
        @test size(sΣx) == (3, 1, 4, 1)
        @test dimsorted(sΣx) == 3

        @test_throws ArgumentError insertaxes(Σx, 3 => 1:1, 2 => 1:1)
    end

    @testset "map" begin
        Σx = sparsesum([1, 2, 3, 2], ones(4))
        bΣx = map(v -> iszero(v) ? zero(BoolSemiring) : one(BoolSemiring), Σx)
        xnzcoo, xnzval = findnz(Σx)
        bnzcoo, bnzval = findnz(bΣx)
        @test Set(bnzcoo) == Set(xnzcoo)
        @test val.(bnzval) == [true, true, true, true]

        sΣx = sort(Σx, 1)
        bΣx = map(v -> iszero(v) ? zero(BoolSemiring) : one(BoolSemiring), sΣx)
        xnzcoo, xnzval = findnz(sΣx)
        bnzcoo, bnzval = findnz(bΣx)
        @test bΣx isa AbstractSortedSumSparseTensor{<:BoolSemiring}
        @test Set(bnzcoo) == Set(xnzcoo)
        @test val.(bnzval) == [true, true, true, true]

        Σx = sparsesum([(1, 2), (2, 3)], ones(S, 2))
        Σy = sparsesum([(1, 2, 3), (2, 3, 4)], ones(S, 2))
        Σz = tensorproduct(Σx, Σy)
        bΣz = map(v -> iszero(v) ? zero(BoolSemiring) : one(BoolSemiring), Σz)
        @test bΣz isa AbstractSumSparseTensor{<:BoolSemiring}

        Σx = sparsesum([(1, 2), (2, 3)], ones(S, 2))
        Σy = sparsesum([(1, 2, 3), (2, 3, 4)], ones(S, 2))
        sΣx = sort(Σx, 1)
        sΣy = sort(Σy, 2)
        sΣz = tensorproduct(sΣx, sΣy)
        bΣz = map(v -> iszero(v) ? zero(BoolSemiring) : one(BoolSemiring), sΣz)
        @test bΣz isa AbstractSumSparseTensor{<:BoolSemiring}

        Σx = sparsesum([(1, 2), (2, 3)], ones(S, 2))
        Σy = sparsesum([(1, 2, 3), (2, 3, 4)], ones(S, 2))
        sΣx = sort(Σx, 1)
        sΣy = sort(Σy, 2)
        sΣz = contract(tensorproduct(sΣx, sΣy), 2, 2)
        bΣz = map(v -> iszero(v) ? zero(BoolSemiring) : one(BoolSemiring), sΣz)
        @test bΣz isa AbstractSortedSumSparseTensor{<:BoolSemiring}
    end

    @testset "mapcoo" begin
        Σx = sparsesum([1, 2, 3, 2], ones(4))
        bΣx = mapcoo(c -> (c[1] - 1,), Σx)
        bnzcoo, bnzval = findnz(bΣx)
        @test all(sort(bnzcoo) .== [(0,), (1,), (1,), (2,)])

        sΣx = sort(Σx, 1)
        bΣx = mapcoo(c -> (c[1] - 1,), sΣx)
        bnzcoo, bnzval = findnz(bΣx)
        @test all(sort(bnzcoo) .== [(0,), (1,), (1,), (2,)])

        Σx = sparsesum([(1, 2), (2, 3)], ones(S, 2))
        Σy = sparsesum([(1, 2, 3), (2, 3, 4)], ones(S, 2))
        sΣx = sort(Σx, 1)
        sΣy = sort(Σy, 2)
        sΣz = copy(contract(tensorproduct(sΣx, sΣy), 2, 2))
        bΣz = mapcoo(c -> (c[1] - 1, c[2:end]...), sΣz)
        nzcoo, nzval = findnz(sΣz)
        bnzcoo, bnzval = findnz(bΣz)
        @test all(sort(bnzcoo) .== sort(map(c -> (c[1] - 1, c[2:end]...), nzcoo)))
    end

    @testset "resize" begin
        Σx = resize(sparsesum([1, 2, 3, 2], ones(S, 4); axis = 1:10), 20)
        @test size(Σx) == (20,)

        Σx = sparsesum([(1, 1)], ones(S, 1); axes = (1:3, 1:3))
        @test size(resize(Σx, (3, 4))) == (3, 4)

        sΣx = resize(sort(Σx, 1), (3, 4))
        @test size(sΣx) == (3, 4)
        @test sΣx isa AbstractSortedSumSparseTensor

        sΣx = resize(sort(Σx, 2), (3, 4))
        @test size(sΣx) == (3, 4)
        @test length(sliceptr(sΣx)) == 5
        @test sΣx isa AbstractSortedSumSparseTensor

        @test_throws ArgumentError resize(Σx, (1, 4))
    end

    @testset "sum" begin
        Σx = sparsesum([(1, 2, 3, 4), (4, 3, 2, 1),], ones(S, 2))
        Σy = sum(Σx, dims = (2, 3))
        @test size(Σy) == (4, 4)
        @test nnz(Σy) ==  nnz(Σx)
    end

    @testset "updateaxes" begin
        Σx = updateaxes(sparsesum([1, 2, 3, 2], ones(S, 4); axis = 1:10), 0:4)
        @test axes(Σx) == (0:10,)

        Σx = sparsesum([(1, 1)], ones(S, 1); axes = (1:3, 1:3))
        @test axes(updateaxes(Σx, (1:3, 1:4))) == (1:3, 1:4)

        sΣx = updateaxes(sort(Σx, 1), (1:3, 1:4))
        @test axes(sΣx) == (1:3, 1:4)
        @test sΣx isa AbstractSortedSumSparseTensor

        sΣx = updateaxes(sort(Σx, 2), (1:3, 0:4))
        @test axes(sΣx) == (1:3, 0:4)
        @test length(sliceptr(sΣx)) == 6
        @test sΣx isa AbstractSortedSumSparseTensor
    end
end

@testset verbose=true failfast=true "linalg" begin
    S = LogSemiring{Float64,1}

    @testset "contract" begin
        Σx = sparsesum([(1, 2), (2, 3)], ones(S, 2))
        Σy = sparsesum([(1, 2, 3), (2, 3, 4)], ones(S, 2))
        sΣx = sort(Σx, 1)
        sΣy = sort(Σy, 2)
        sΣz = contract(tensorproduct(sΣx, sΣy), 2, 2)
        @test sΣz isa AbstractSortedSumSparseTensor{S,3,1}
        nzcoo, _ = findnz(sΣz)

        nzcoo, _ = findnz(copy(sΣz))
        @test Set(nzcoo) == Set([(1, 1, 3), (2, 2, 4)])

        Σx = sparsesum([(1, 2, 2, 2), (2, 3, 3, 3)], ones(S, 2); axes = (1:3, 1:3, 1:3, 1:3))
        Σy = sparsesum([(1, 2, 2, 2)], ones(S, 1); axes = (1:2, 1:2, 1:2, 1:2))
        sΣx = sort(Σx, 1)
        sΣy = sort(Σy, 2)
        @test_throws DimensionMismatch contract(tensorproduct(sΣx, sΣy), 2, 2)


        Σx = sparsesum([(1, 2, 2, 2), (2, 3, 3, 3), (1, 3, 2, 3)], ones(S, 3); axes = (1:3, 1:3, 1:3, 1:3))
        Σy = sparsesum([(1, 2, 2, 2)], ones(S, 1); axes = (1:2, 1:2, 1:3, 1:3))
        sΣx = sort(Σx, 1)
        sΣy = sort(Σy, 3)
        C = contract(tensorproduct(sΣx, sΣy), 4, 3)
        @test size(C) == (3, 3, 3, 2, 2, 3)

        nzcoo, _ = findnz(copy(C))
        @test Set(nzcoo) == Set([(1, 2, 2, 1, 2, 2)])

        @test all(mulsum(ones(S, 3, 3), C, (1, 2), (3, 4)) .== [zero(S) zero(S); one(S) zero(S); zero(S) zero(S)])
    end

    @testset "dot" begin
        Σx = sparsesum([1, 2, 3, 2, 1], ones(S, 5))
        y = ones(S, 3)
        @test val(dot(y, Σx)) ≈ val(5*one(S))
        @test val(dot(Σx, y)) ≈ val(5*one(S))
    end

    @testset "dotstar" begin
        ΣA = sparsesum([(1, 2), (1, 3), (2, 3)], ones(S, 3); axes = (1:3, 1:3))
        Σx = sparsesum([1], ones(S, 1); axis = 1:3)
        Σy = sparsesum([3], ones(S, 1); axis = 1:3)
        w = dotstar(Σx, ΣA, Σy, (1,), (2,))
        @test val(w) .== val(2*one(S))


        ΣA = sparsesum([(1, 2, 2, 1, 2, 2), (2, 1, 3, 3, 3, 3), (1, 2, 3, 3, 2, 3)], ones(S, 3); axes = (1:3, 1:3, 1:3, 1:3, 1:3, 1:3))
        Σx = sparsesum([(1, 2)], ones(S, 1); axes = (1:3, 1:3))
        Σy = sparsesum([(3, 3)], ones(S, 1); axes = (1:3, 1:3))
        w = dotstar(Σx, ΣA, Σy, (1, 2), (3, 4))
        @test val(w) .== val(2*one(S))
    end

    @testset "add" begin
        Σx = sparsesum([(1, 1)], ones(S, 1); axes = (1:3, 1:3))
        Σy = sparsesum([(2, 2)], ones(S, 1); axes = (1:3, 1:3))
        Σz = add(Σx, Σy)
        nzcoo, nzval = findnz(Σz)
        @test Set(nzcoo) == Set([(1, 1), (2, 2)])
        @test all(val.(nzval) .== val(one(S)))

        z = add(Σx, ones(S, size(Σx)...))
        @test z[1,1] == 2 * one(S)
        @test z[2,2] == one(S)
        @test z[1,2] == one(S)
        @test z[2,1] == one(S)
        @test all(z .== add(ones(S, size(Σx)...), Σx))

        Σx = sparsesum([2], ones(S, 1); axis = 1:3)
        y = add(Σx, 2 .* ones(S, 3))
        z = add(2 .* ones(S, 3), Σx)
        @test all(val.(y) .≈ val.([2*one(S), 3*one(S), 2*one(S)]))
        @test all(val.(z) .≈ val.([2*one(S), 3*one(S), 2*one(S)]))


        x = add(ones(S, 3, 3), ones(S, 3, 3))
        @test all(val.(x) .== [val(2*one(S))])
    end

    @testset "mul" begin
        x = [zero(S) zero(S); zero(S) 2*one(S)]
        u = [zero(S) zero(S); one(S) one(S)]
        Σy = sparsesum([(1, 2, 1), (2, 2, 1)], ones(S, 2); axes = (1:2, 1:2, 1:1))
        Σz = mul(x, Σy, (1, 2))
        nzcoo, nzval = findnz(Σz)
        @test nnz(Σz) == 1
        @test Set(nzcoo) == Set([(2, 2, 1)])
        @test val(nzval[1]) == val(2*one(S))

        Σz = mul(x, Σy, u, (1, 2), (1, 2))
        nzcoo, nzval = findnz(Σz)
        @test nnz(Σz) == 1
        @test Set(nzcoo) == Set([(2, 2, 1)])
        @test val(nzval[1]) == val(2*one(S))

        sΣz = mul(x, sort(Σy, 1), (1, 2))
        nzcoo, nzval = findnz(sΣz)
        @test sΣz isa AbstractSortedSumSparseTensor
        @test nnz(sΣz) == 1
        @test all(sliceptr(sΣz) .== [1, 1, 2])
        @test Set(nzcoo) == Set([(2, 2, 1)])
        @test val(nzval[1]) == val(2*one(S))

        sΣz = mul(x, sort(Σy, 1), u, (1, 2), (1, 2))
        nzcoo, nzval = findnz(sΣz)
        @test sΣz isa AbstractSortedSumSparseTensor
        @test nnz(sΣz) == 1
        @test all(sliceptr(sΣz) .== [1, 1, 2])
        @test Set(nzcoo) == Set([(2, 2, 1)])
        @test val(nzval[1]) == val(2*one(S))
    end

    @testset "tensorproduct" begin
        Σx = sparsesum([1, 3], ones(S, 2); axis = 1:3)
        Σy = sparsesum([2, 3], 2 .* ones(S, 2); axis = 1:3)
        Σz = tensorproduct(Σx, Σy)
        nzcoo, nzval = findnz(Σz)
        @test Set(nzcoo) == Set([(1, 2), (1, 3), (3, 2), (3, 3)])
        @test all(val.(nzval) .== val(2 * one(S)))

        Σx = sparsesum([(1, 2), (2, 3)], ones(S, 2))
        Σy = sparsesum([(1, 2, 3), (2, 3, 4)], ones(S, 2))
        Σz = tensorproduct(Σx, Σy)
        @test nnz(Σz) == nnz(Σx) * nnz(Σy)
        nzcoo, _ = findnz(Σz)
        @test Set(nzcoo) == Set([(1, 2, 1, 2, 3), (1, 2, 2, 3, 4),
                                     (2, 3, 1, 2, 3), (2, 3, 2, 3, 4)])
        @test dot(ones(S, size(Σz)...), Σz) == nnz(Σz) * one(S)
        @test dot(Σz, ones(S, size(Σz)...)) == nnz(Σz) * one(S)

        sΣx = sort(Σx, 1)
        sΣy = sort(Σy, 2)
        sΣz = tensorproduct(sΣx, sΣy)
        @test sΣz isa AbstractSortedSumSparseTensor{S,5,1}
        @test nnz(sΣz) == nnz(Σx) * nnz(Σy)
        nzcoo, _ = findnz(sΣz)
        @test Set(nzcoo) == Set([(1, 2, 1, 2, 3), (1, 2, 2, 3, 4),
                                 (2, 3, 1, 2, 3), (2, 3, 2, 3, 4)])

    end

    @testset "mulsum" begin
        ΣA = sparsesum([(1, 2), (1, 3), (2, 3)], ones(S, 3); axes = (1:3, 1:3))
        y = mulsum(ones(S, 3), ΣA, (1,), (2,))
        @test all(val.(y) .== val.([zero(S), one(S), 2*one(S)]))
        y = mulsum(ΣA, ones(S, 3), (1,), (2,))
        @test all(val.(y) .== val.([zero(S), one(S), 2*one(S)]))

        y = mulsum(ones(S, 3), sort(ΣA, 1), (1,), (2,))
        @test all(val.(y) .== val.([zero(S), one(S), 2*one(S)]))
        y = mulsum(sort(ΣA, 1), ones(S, 3), (1,), (2,))
        @test all(val.(y) .== val.([zero(S), one(S), 2*one(S)]))

        y = mulsum(ones(S, 3), sort(ΣA, 2), (1,), (2,))
        @test all(val.(y) .== val.([zero(S), one(S), 2*one(S)]))
        y = mulsum(sort(ΣA, 2), ones(S, 3), (1,), (2,))
        @test all(val.(y) .== val.([zero(S), one(S), 2*one(S)]))
    end

    @testset "mulsumstar" begin
        ΣA = sparsesum([(1, 2), (1, 3), (2, 3)], ones(S, 3); axes = (1:3, 1:3))
        Σx = sparsesum([1], ones(S, 1); axis = 1:3)
        y = mulsumstar(Σx, ΣA, (1,), (2,))
        @test all(val.(y) .== val.([one(S), one(S), 2*one(S)]))
        y = mulsumstar(ΣA, Σx, (1,), (2,))
        @test all(val.(y) .== val.([one(S), one(S), 2*one(S)]))

        ΣA = sparsesum([(1, 2, 2, 1, 2, 2), (2, 1, 3, 3, 3, 3), (1, 2, 3, 3, 2, 3)], ones(S, 3); axes = (1:3, 1:3, 1:3, 1:3, 1:3, 1:3))
        Σx = sparsesum([(1, 2)], ones(S, 1); axes = (1:3, 1:3))
        y = mulsumstar(Σx, ΣA, (1, 2), (3, 4))
        ŷ = [zero(S) one(S) zero(S); one(S) zero(S) zero(S); zero(S) zero(S) 2*one(S)]
        @test all(val.(y) .== val.(ŷ))
        y = mulsumstar(ΣA, Σx, (1, 2), (3, 4))
        ŷ = [zero(S) one(S) zero(S); one(S) zero(S) zero(S); zero(S) zero(S) 2*one(S)]
        @test all(val.(y) .== val.(ŷ))

        # test that the algorithm does converge for the BoolSemiring.
        S = BoolSemiring
        ΣA = sparsesum([(1, 1), (1, 2), (1, 3), (2, 3)], ones(S, 4); axes = (1:3, 1:3))
        Σx = sparsesum([1], ones(S, 1); axis = 1:3)
        y = mulsumstar(Σx, ΣA, (1,), (2,))
        @test all(val.(y) .== val.([one(S), one(S), 2*one(S)]))
        y = mulsumstar(ΣA, Σx, (1,), (2,))
        @test all(val.(y) .== val.([one(S), one(S), 2*one(S)]))
    end
end

@testset "Nested contraction with tensordot" begin
    S = BoolSemiring
    a = sparsesum([(1,)], ones(S, 1))
    b = sparsesum([(1,)], ones(S, 1))
    c = tensorproduct(a, b)
    d = tensorproduct(a, c)
    @test add(zeros(S, 1, 1, 1), d) != nothing # Add was failing

    a = sort(sparsesum([(1, 1)], ones(S, 1); axes=(1:2, 1:2)), 2)
    b = sort(sparsesum([(1, 2), (2, 1)], ones(S, 2)), 1)
    c = contract(tensorproduct(a, b), 1, 1)
    d = contract(tensorproduct(c, b), 1, 1)
    nzcoo, nzval = findnz(d)
    idx = first(nzinds(d, 1))

    @test nzcoo[idx] == (2, 2) # getindex was failing due to wrong idx
end

