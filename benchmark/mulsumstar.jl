
using Revise

using Semirings
using SumSparseTensors
using BenchmarkTools

include("common.jl")


function benchmark_mulsumstar(
        num_states::Int64 = 100,
        seconds::Float64 = 2.;
        S = LogSemiring{Float64,1},
)
    ΣA = generate_acyclic_graph(num_states, S)  # common.jl
    Σx = sparsesum([1], ones(S, 1); axis = 1:num_states)

    println("$(S), num_states = $(num_states), nnz(ΣA) = $(nnz(ΣA)), seconds=$(seconds)")

    t_xA = @benchmark mulsumstar($Σx, $ΣA, (1,), (2,)) seconds=seconds
    println("t_xA : mulsumstar(Σx, ΣA, ...): $(t_xA)")

    t_Ax = @benchmark mulsumstar($ΣA, $Σx, (1,), (2,)) seconds=seconds
    println("t_Ax : mulsumstar(ΣA, Σx, ...): $(t_Ax)")
    println()

    return (t_xA, t_Ax)
end

println("### BENCHMARKING ###\n")

t_xA_log, t_Ax_log = benchmark_mulsumstar(500; S=LogSemiring{Float64,1})  # 436ms, 192MB
display(t_xA_log)
display(t_Ax_log)

println("\n--------------------\n")

t_xA_trop, t_Ax_trop = benchmark_mulsumstar(500; S=TropicalSemiring{Float64})  # 38ms, 18MB
display(t_xA_trop)
display(t_Ax_trop)

println("\n--------------------\n")
println("Output `Trial` in variables: t_xA_log, t_Ax_log, t_xA_trop, t_Ax_trop")


