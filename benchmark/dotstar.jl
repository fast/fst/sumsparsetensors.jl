
using Revise

using Semirings
using SumSparseTensors
using BenchmarkTools

include("common.jl")


function benchmark_dotstar(
        num_states = 100,
        seconds = 2. ;
        S = LogSemiring{Float64,1},
)
    ΣA = generate_acyclic_graph(num_states, S)  # common.jl
    Σx = sparsesum([1], ones(S, 1); axis = 1:num_states)
    Σy = sparsesum([num_states], ones(S, 1); axis = 1:num_states)

    println("$(S), num_states=$(num_states), nnz(ΣA) = $(nnz(ΣA)), seconds = $(seconds)")

    t = @benchmark dotstar($Σx, $ΣA, $Σy, (1,), (2,)) seconds=seconds

    println("dotstar(Σx, ΣA, Σy, ...): $(t)")
    println()

    return t
end

println("### BENCHMARKING ###\n")

t_log = benchmark_dotstar(500; S=LogSemiring{Float64,1})  # 748ms, 320MB
display(t_log)

println("\n--------------------\n")

t_tropical = benchmark_dotstar(500; S=TropicalSemiring{Float64})  # 724ms, 324MB
display(t_tropical)

println("\n--------------------\n")
println("Output `Trial` in variables: t_log, t_tropical")
