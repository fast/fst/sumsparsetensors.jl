
using Semirings
using SumSparseTensors

function generate_acyclic_graph(num_states::Int64, S)

    # links = AbstractArray{Tuple}[]
    links = Tuple{Int64,Int64}[]
    for ii in 1:(num_states-1)
        # linear path
        push!(links, (ii, ii+1))

        # skip-links
        # * skip no more than 20 states
        # * E[sum(mask == true)] = 4
        # -> on average 5 outgoing links
        mask = rand(20) .> 0.8
        nonzero_indices = findall(x -> x > 0, mask)
        for ni in nonzero_indices
            tgt = ii + 1 + ni
            if tgt <= num_states
                push!(links, (ii, tgt))
            end
        end
    end
    num_links = size(links)[1]

    weights = ones(S, num_links)

    ΣA = sparsesum(links, weights; axes = (1:num_states, 1:num_states))

    return ΣA
end


