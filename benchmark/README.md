# Benchmark functions for ../src/SumSparseTensors.jl

run activate dedicated environment:
```
cd FST/sumsparsetensors.jl
julia
pkg> activate benchmark
pkg> registry add "https://gitlab.lisn.upsaclay.fr/fast/registry"
pkg> update
pkg> dev .
```

measure time and memory consumption
```
include("benchmark/mulsumstar.jl")
include("benchmark/dotstar.jl")
```

the graph topology is created by : `generate_acyclic_graph()` from `benchmark/common.jl`


## Output example

```
julia> include("benchmark/dotstar.jl")

### BENCHMARKING ###

LogSemiring{Float64, 1}, num_states=500, nnz(ΣA) = 2471, seconds = 2.0
dotstar(Σx, ΣA, Σy, ...): Trial(744.321 ms)

BenchmarkTools.Trial: 3 samples with 1 evaluation.
 Range (min … max):  744.321 ms … 746.385 ms  ┊ GC (min … max): 1.14% … 1.15%
 Time  (median):     745.697 ms               ┊ GC (median):    1.15%
 Time  (mean ± σ):   745.468 ms ±   1.051 ms  ┊ GC (mean ± σ):  1.20% ± 0.10%

  █                                       █                   █
  █▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁█▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁█ ▁
  744 ms           Histogram: frequency by time          746 ms <

 Memory estimate: 321.76 MiB, allocs estimate: 15592142.

--------------------

TropicalSemiring{Float64}, num_states=500, nnz(ΣA) = 2440, seconds = 2.0
dotstar(Σx, ΣA, Σy, ...): Trial(702.712 ms)

BenchmarkTools.Trial: 3 samples with 1 evaluation.
 Range (min … max):  702.712 ms … 714.068 ms  ┊ GC (min … max): 1.22% … 1.19%
 Time  (median):     703.268 ms               ┊ GC (median):    1.21%
 Time  (mean ± σ):   706.683 ms ±   6.402 ms  ┊ GC (mean ± σ):  1.21% ± 0.02%

  █ █                                                         █
  █▁█▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁█ ▁
  703 ms           Histogram: frequency by time          714 ms <

 Memory estimate: 317.73 MiB, allocs estimate: 15390239.

--------------------

Output `Trial` in variables: t_log, t_tropical

```
