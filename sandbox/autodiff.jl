### A Pluto.jl notebook ###
# v0.19.32

using Markdown
using InteractiveUtils

# ╔═╡ 7ca29aea-9c29-11ee-0f4b-730f7167b6c7
begin
	using Pkg
	Pkg.activate("..")

	using Revise
	using Semirings
	using SumSparseTensors

	using ChainRulesCore
	using LinearAlgebra
	using Zygote
end

# ╔═╡ 20ee4df9-6124-4185-9f3f-fd75f361ff94
S = LogSemiring{Float64,1}

# ╔═╡ 4f71d25b-580a-482f-86f9-387e0c4db5de
α = sparse([1, 2], S[one(S)]; size = 3)

# ╔═╡ f0beacc1-98e1-4178-8548-65706de13789
ω = sparse([3], S[one(S)]; size = 3)

# ╔═╡ c4e779af-1d0c-4fd2-aca3-4a4f6fd87cab
A = sparse([1, 1, 2, 2, 3], [1, 2, 2, 3, 3], ones(S, 5))

# ╔═╡ 8e2a3c64-442a-4187-af6e-18d940e18b03
B = sparse([1, 2, 1], [2, 3, 3], S[1, 2, 3])

# ╔═╡ 03e9b009-5283-4214-9c14-67a4217ce1ac
md"""
```math
w = \boldsymbol{\alpha}^\top \mathbf{A}^* \boldsymbol{\omega}
```

```math
\begin{align}
\mathbf{u}_0 &= \boldsymbol{\alpha} \\
\mathbf{u}_{n+1} &= \mathbf{u}_{n}^\top \mathbf{A}
\end{align}
```

```math
w = \sum_n \mathbf{u}_n^\top \boldsymbol{\omega}
```
"""

# ╔═╡ 5e7ee099-b836-48e5-a0a0-637c2a898826
function w(A, α, ω; N=A.size[2])
	uₙ = zeros(S, α.size) + α
	res = dot(uₙ, ω)
	for n in 1:N
		uₙ = vecmatmul(uₙ, A)
		res = (res ⊕ dot(uₙ, ω))
	end
	res
end

# ╔═╡ 097f0ec8-f616-4eac-b10a-f8ed0065fe33
w(A, α, ω; N=3)

# ╔═╡ b96e2519-5a1d-4b20-a72b-abfc34abdbf3
gradient(A, α, ω) do X, y, z
	val(w(X, y, z; N = 3))
end

# ╔═╡ 93db3d7a-e0cc-4900-9fe7-6bbd4377d713
gradient(B, α, ω) do X, y, z
	val(w(X, y, z; N = 10))
end

# ╔═╡ e9e58390-6def-4374-80f7-da866770a991
Yota.grad(A, α, ω) do X, y, z
	val( w(X,y,z;N=1) )
end

# ╔═╡ defcdd48-7c47-4d59-9b6c-25b2dc7270b9
A.I, A.J

# ╔═╡ 258205a5-e269-4688-8a90-fa1317afeb14
begin
	const I = [1, 1, 1, 1, 1, 2, 2, 2]
 	const J = [1, 1, 2, 2, 2, 2, 2, 2]
 	const K = [1, 1, 1, 1, 2, 2, 2, 2]
 	const L = [2, 3, 1, 3, 1, 1, 2, 3]

	x = one(S) ⊕ one(S)
	const V = S[x, 2x, 3x, 4x, 5x, 6x, 7x, 8x]
end

# ╔═╡ 65625c73-d93e-4755-a93f-4f195b1c19e6
X = sparse(I, J, K, L, V)

# ╔═╡ 030ded8b-863d-47ac-96ff-6dd2161a054a
y = sparse(L, V; size=4)

# ╔═╡ Cell order:
# ╠═7ca29aea-9c29-11ee-0f4b-730f7167b6c7
# ╠═20ee4df9-6124-4185-9f3f-fd75f361ff94
# ╠═4f71d25b-580a-482f-86f9-387e0c4db5de
# ╠═f0beacc1-98e1-4178-8548-65706de13789
# ╠═c4e779af-1d0c-4fd2-aca3-4a4f6fd87cab
# ╠═8e2a3c64-442a-4187-af6e-18d940e18b03
# ╟─03e9b009-5283-4214-9c14-67a4217ce1ac
# ╠═5e7ee099-b836-48e5-a0a0-637c2a898826
# ╠═097f0ec8-f616-4eac-b10a-f8ed0065fe33
# ╠═b96e2519-5a1d-4b20-a72b-abfc34abdbf3
# ╠═93db3d7a-e0cc-4900-9fe7-6bbd4377d713
# ╠═e9e58390-6def-4374-80f7-da866770a991
# ╠═defcdd48-7c47-4d59-9b6c-25b2dc7270b9
# ╠═258205a5-e269-4688-8a90-fa1317afeb14
# ╠═65625c73-d93e-4755-a93f-4f195b1c19e6
# ╠═030ded8b-863d-47ac-96ff-6dd2161a054a
