### A Pluto.jl notebook ###
# v0.19.36

using Markdown
using InteractiveUtils

# ╔═╡ d3c7b620-9dbc-11ee-2091-33f7958a87da
begin
	using Pkg
	Pkg.activate("..")

	using Revise
	using LinearAlgebra
	using SumSparseTensors
	using Semirings
	using BenchmarkTools
end

# ╔═╡ 5e42c800-fa52-4c55-b692-161c622e9da9
S = ProbSemiring{Float64}

# ╔═╡ 93cc1b4a-b928-4509-a997-a69de0d262cd
ΣA = sparsesum(
	[
		(1, 2, 3, 3),
		(2, 1, 3, 3),
		(2, 3, 3, 3),
		(2, 4, 3, 3),
		(3, 4, 3, 3),
		(4, 4, 2, 2)
	], 
	S[0.1, 0.2, 0.3, 0.4, 0.5, 0.6]; 
	size = (4, 4, 3, 3)
)

# ╔═╡ e04a0436-9746-4e0f-9f21-84537043568b
ΣB = sparsesum(
	[
		(1, 2, 3, 3),
		(2, 2, 3, 3),
		(2, 3, 2, 2),
		(2, 4, 2, 2),
		(3, 4, 3, 3)
	], 
	S[0.1, 0.2, 0.3, 0.4, 0.5]; 
	size = (4, 4, 3, 3)
)

# ╔═╡ ceee007c-5c20-4114-8d10-04badaf6c59f
TP = tensorproduct(ΣA, ΣB) 

# ╔═╡ 499bc1f3-55f9-494b-bf5c-62825b6cf10c
sTP = tensorproduct(sort(ΣA, 1), sort(ΣB, 3)) 

# ╔═╡ 5c67c7b0-fa51-4669-875a-f9f6d49c98f0
findnz(TP) 

# ╔═╡ 6a24aa8e-227e-4880-869e-5a84f3d388cc
sum(TP, dims = (3, 4, 5, 6, 7, 8)) 

# ╔═╡ 49c28352-cfe7-4cd8-b46d-1d33f43f958a
C = contract(sTP, 4, 3)

# ╔═╡ 680625df-e32f-407b-bac9-59634f38fc31
size(C.TP)

# ╔═╡ e7745e4e-2ca0-4a35-863e-caf4ed93b483
findnz(C.TP)

# ╔═╡ 1c0c4977-ed76-407c-a54e-d08791743457
nzinds(C.TP, 1)

# ╔═╡ 13a1100f-55d8-4b74-bdf9-79ad8ca69b28
size(C)

# ╔═╡ d881883c-0799-4baa-a8a2-b221f0b843cf
nzi = (1, 2)

# ╔═╡ 238d23a4-14d9-4b07-ad43-dfe87716c749
(nzi...,)

# ╔═╡ aced7041-170f-48e6-9dbe-6b7da07e13d8
 first(nzinds(C, 1)) |> length

# ╔═╡ ed5382d1-40df-4606-903d-d0d6c0223b27
for nzi in nzinds(C, 4)
	println(nzi, " ", findnz(C)[1][nzi])
end

# ╔═╡ 6be95448-f902-4f63-9e0c-fd81f945cf7a
size(ones(1, 2, 3, 4))[(1, 2)...]

# ╔═╡ 820d9062-5d1f-4299-8581-88e42e566e3d
md"""
```math
y_{kl} = \sum_{ijmn} x_{ij} A_{kiljmn}
```
"""

# ╔═╡ Cell order:
# ╠═d3c7b620-9dbc-11ee-2091-33f7958a87da
# ╠═5e42c800-fa52-4c55-b692-161c622e9da9
# ╠═93cc1b4a-b928-4509-a997-a69de0d262cd
# ╠═e04a0436-9746-4e0f-9f21-84537043568b
# ╠═ceee007c-5c20-4114-8d10-04badaf6c59f
# ╠═499bc1f3-55f9-494b-bf5c-62825b6cf10c
# ╠═5c67c7b0-fa51-4669-875a-f9f6d49c98f0
# ╠═6a24aa8e-227e-4880-869e-5a84f3d388cc
# ╠═49c28352-cfe7-4cd8-b46d-1d33f43f958a
# ╠═680625df-e32f-407b-bac9-59634f38fc31
# ╠═e7745e4e-2ca0-4a35-863e-caf4ed93b483
# ╠═1c0c4977-ed76-407c-a54e-d08791743457
# ╠═13a1100f-55d8-4b74-bdf9-79ad8ca69b28
# ╠═d881883c-0799-4baa-a8a2-b221f0b843cf
# ╠═238d23a4-14d9-4b07-ad43-dfe87716c749
# ╠═aced7041-170f-48e6-9dbe-6b7da07e13d8
# ╠═ed5382d1-40df-4606-903d-d0d6c0223b27
# ╠═6be95448-f902-4f63-9e0c-fd81f945cf7a
# ╠═820d9062-5d1f-4299-8581-88e42e566e3d
