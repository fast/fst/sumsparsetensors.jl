# Releases

## [0.14.0](https://gitlab.lisn.upsaclay.fr/fast/fst/SumSparseTensors.jl/-/tags/v0.14.0) - 01.03.2024
### Added
- Differentiation rules for mulsum

## [0.13.2](https://gitlab.lisn.upsaclay.fr/fast/fst/SumSparseTensors.jl/-/tags/v0.13.2) - 22.02.2024
### Fixed
- nested contraction fails to handle tuple indices

## [0.13.1](https://gitlab.lisn.upsaclay.fr/fast/fst/SumSparseTensors.jl/-/tags/v0.13.1) - 20.02.2024
### Fixed
- `copy` fails when having coordinates with negative index along the sorted dimension

## [0.13.0](https://gitlab.lisn.upsaclay.fr/fast/fst/SumSparseTensors.jl/-/tags/v0.13.0) - 19.02.2024
### Added
- `mapcoo` function

## [0.11.1](https://gitlab.lisn.upsaclay.fr/fast/fst/SumSparseTensors.jl/-/tags/v0.11.1) - 17.02.2024
### Improved
- `Base.cat` check the axes of the argument rather than the size of each
   dimension. Consequently, concatenation over dimensions having different
   range raises an error

## [0.11.0](https://gitlab.lisn.upsaclay.fr/fast/fst/SumSparseTensors.jl/-/tags/v0.11.0) - 14.02.2024
### Changed
- `fiberptr` has been renamed `sliceptr`
- `size` of the tensor is replaced by `axes`, a tuple of ranges for each
  dimension

## [0.10.0](https://gitlab.lisn.upsaclay.fr/fast/fst/SumSparseTensors.jl/-/tags/v0.10.0) - 07.02.2024
### Changed
- the non-zero values and coordinates of the (sum of) sparse tensors can be
  stored in n-dimensional array

## [0.9.0](https://gitlab.lisn.upsaclay.fr/fast/fst/SumSparseTensors.jl/-/tags/v0.9.0) - 06.02.2024
### Added
- `map` function to transform the non-zero values of a sum of sparse tensors
### Changed
- `mulsum` and `mulsumstar` accept different order of argument for left and
  right multiplication
- `mul` requires dimensions specifications
- `mul` has an optional right-side argument
- star based operations monitor the convergence by checking if there is no
  change in the result between two iterations

## [0.8.0](https://gitlab.lisn.upsaclay.fr/fast/fst/SumSparseTensors.jl/-/tags/v0.8.0) - 05.02.2024
### Added
- `mulsumstar` operation

## [0.7.0](https://gitlab.lisn.upsaclay.fr/fast/fst/SumSparseTensors.jl/-/tags/v0.7.0) - 05.02.2024
### Added
- `Base.copy` materialize the lazy operations, i.e. `tensorproduct` and `contract`
### Fixed
- `dot` failing for tensor products
- `mulsum` failing on contracted tensor products

## [0.6.0](https://gitlab.lisn.upsaclay.fr/fast/fst/SumSparseTensors.jl/-/tags/v0.6.0) - 02.02.2024
### added
- elementwise multiplication between a dense tensor and a sum of sparse
  tensors.

## [0.5.4](https://gitlab.lisn.upsaclay.fr/fast/fst/SumSparseTensors.jl/-/tags/v0.5.4) - 02.02.2024
### fixed
- size of contracted tensor is erroneou
- contract does not occur over the specified indices

## [0.5.3](https://gitlab.lisn.upsaclay.fr/fast/fst/SumSparseTensors.jl/-/tags/v0.5.3) - 01.02.2024
### fixed
- resizing sorted tensors do no update their fiber pointers
- `contract` does not check that the contraction dimension have the same length

## [0.5.2](https://gitlab.lisn.upsaclay.fr/fast/fst/SumSparseTensors.jl/-/tags/v0.5.2) - 01.02.2024
### fixed
- inplace addition sum of tensors with more than 1 dimensions
- `mulsum` with sorted tensors

## [0.5.1](https://gitlab.lisn.upsaclay.fr/fast/fst/SumSparseTensors.jl/-/tags/v0.5.1) - 01.02.2024
### fixed
- adding sum of tensors with more than 1 dimensions

## [0.5.0](https://gitlab.lisn.upsaclay.fr/fast/fst/SumSparseTensors.jl/-/tags/v0.5.0) - 01.02.2024
### Added
- `insertaxes` function to add axes at a specific dimension

## [0.4.0](https://gitlab.lisn.upsaclay.fr/fast/fst/SumSparseTensors.jl/-/tags/v0.4.0) - 30.01.2024
### Added
- sum of tensors supports the `eltype` function

## [0.3.0](https://gitlab.lisn.upsaclay.fr/fast/fst/SumSparseTensors.jl/-/tags/v0.3.0) - 29.01.2024
### Added
- contraction of tensor product
### Changed
- dot product and vector-matrix multiplication are generalized to arbitrary
  shapes (`vecmatmul` is renamed to `mulsum`)

## [0.2.1](https://gitlab.lisn.upsaclay.fr/fast/fst/SumSparseTensors.jl/-/tags/v0.2.1) - 11.01.2024
### Fixed
- not copying the non-zero values while concatenating tensors

## [0.2.0](https://gitlab.lisn.upsaclay.fr/fast/fst/SumSparseTensors.jl/-/tags/v0.2.0) - 11.01.2024
- generic `n`-dimensional sum of sparse tensor with coordinate format
- base and linear algebra operations

## [0.1.0](https://gitlab.lisn.upsaclay.fr/fast/fst/SumSparseTensors.jl/-/tags/v0.1.0) - 24.11.2023
- first version extracted from `TensorFSTs.jl`

