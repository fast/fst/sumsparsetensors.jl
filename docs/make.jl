
push!(LOAD_PATH, "..")

using Documenter
using SumSparseTensors

makedocs(
    sitename = "SumSparseTensors.jl",
    repo = Remotes.GitLab("gitlab.lisn.upsaclay.fr", "fast/fst", "SumSparseTensors.jl"),
    format = Documenter.HTML(prettyurls = false),
    pages = [
        "Home" => "index.md",
        "Usage" => "usage.md",
        "API" => "api.md"
    ],
    modules = [SumSparseTensors]
)
