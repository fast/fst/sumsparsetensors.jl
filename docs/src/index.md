# Sum of Sparse Tensors

The [SumSparseTensors.jl](https://gitlab.lisn.upsaclay.fr/fast/fst/sumsparsetensors.jl)
is a Julia package for manipulating sum of sparse tensors whose elements are
semiring values. It is the algorithmic core of the
[TensorFSTs.jl package ](https://gitlab.lisn.upsaclay.fr/fast/fst/TensorFSTs.jl).

## Install

This package is part of the [FAST](https://gitlab.lisn.upsaclay.fr/fast) tool
collection  and requires the [FAST registry](https://gitlab.lisn.upsaclay.fr/fast/registry)
to be installed. To install the registry type:
```julia
pkg> registry add "https://gitlab.lisn.upsaclay.fr/fast/registry"
```
in the package mode of the Julia REPL. You can then install SumSparseTensors.jl
package with the command
```
pkg> add SumSparseTensors
```

## Contact

If you have any questions please contact
[Lucas ONDEL YANG](mailto:lucas.ondel-yang@lisn.upsaclay.fr).


