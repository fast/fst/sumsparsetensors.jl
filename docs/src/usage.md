# Usage

The SumSparseTensors.jl package provides a programming interface to manipulate
sum of sparse tensors such as
```math
\mathbf{x} = \mathbf{x}_1 + \mathbf{x}_2 + ... + \mathbf{x}_K
```
Each of the tensors have elements in a semiring, therefore sum of two tensors
is defined as
```math
(\mathbf{x}_1 + \mathbf{x}_2)_i = x_{1,i} \oplus x_{2,i} \; \forall i \in I
```
where ``\oplus`` is the semiring addition and ``i`` is an element of the index
set ``I``. Fundamentally, these sums are represented as sparse arrays that can
have multiple non-zero values for one coordinate - each of these entries
corresponds to one non-zero element in one of the tensor of the sum.

## Creating a sum of sparse tensor

First you need to define the type of the tensors' elements from the
[Semirings.jl](https://gitlab.lisn.upsaclay.fr/fast/fst/semirings.jl) package.
In this example we will use the log-semiring:
```julia-repl
julia> S = LogSemiring{Float64, 1}
LogSemiring{Float64, 1}
```
Then, you can create a new sum with the [`sparsesum`](@ref) function
```julia-repl
julia> Σx = sparsesum([(1, 1), (1, 2), (1, 2)], [S(1), S(2), S(3)]; size = (3, 3))
SumSparseMatrix{LogSemiring{Float64, 1}}((3, 3), [(1, 1), (1, 2), (1, 2)], LogSemiring{Float64, 1}[1.0, 2.0, 3.0])
```

For optimization purposes, some operations require the sum of tensor to be
sorted, i.e. the non-zero elements are sorted in one of the dimension.
To create a sorted sum you can use the [`sort`](@ref) on an exisiting
sum
```julia-repl
julia> Σx = sparsesum([(1, 2, 3), (2, 1, 3), (3, 2, 1)], ones(S, 3))
SumSparseTensor{LogSemiring{Float64, 1}, 3}((3, 2, 3), [(1, 2, 3), (2, 1, 3), (3, 2, 1)], LogSemiring{Float64, 1}[0.0, 0.0, 0.0])

julia> sorted_Σx = sort(Σx, 2)
SumSparseTensors.SortedSumSparseTensor{LogSemiring{Float64, 1}, 3, 2}((3, 2, 3), [1, 2, 4], [(2, 1, 3), (1, 2, 3), (3, 2, 1)], LogSemiring{Float64, 1}[0.0, 0.0, 0.0])

julia> nzcoo(sorted_Σx)
3-element Vector{Tuple{Int64, Int64, Int64}}:
 (2, 1, 3)
 (1, 2, 3)
 (3, 2, 1)
```

## Sum of tensor interface

Every sum of sparse tensors supports the following functions. Note however
that some function only applies to sorted sums.

### Accessing the non-zero elements

| Function | Require sorted |
|:---------|:---------------|
| [`dimsorted(Σx)`](@ref) | yes |
| [`fiberptr(Σx)`](@ref) | yes |
| [`nnz(Σx)`](@ref) | no |
| [`nzcoo(Σx)`](@ref) | no |
| [`nzrange(Σx)`](@ref) | no |
| [`nzrange(Σx, fiber)`](@ref) | yes |
| [`nzval(Σx)`](@ref) | no |
| [`size(Σx, dim)`](@ref) | no |

### Basic operations

| Function | Require sorted |
|:---------|:---------------|
| [`addaxes(Σx,n)`](@ref) | no |
| [`cat(Σx, Σy; dims)`](@ref) | no |
| [`collapse(Σx)`](@ref) | no |
| [`mapcoo(f, Σx)`](@ref) | no |
| [`mapcoo_sorted(f, Σx)`](@ref) | yes |
| [`resize(Σx, newsize)`](@ref) | no |
| [`sum(Σx; dims)`](@ref) | no |
| [`vcat(Σx, Σy)`](@ref) | no |

### Linear algebra operations

| Function | Require sorted |
|:---------|:---------------|
| [`dot(Σx, y)`](@ref) | no |
| [`dot(y, Σx)`](@ref) | no |
| [`dotstar(Σx, ΣA, Σy)`](@ref) | no |
| [`+(Σx, Σy)`](@ref) | no |
| [`outer(Σx, Σy)`](@ref) | no |
| [`tensorproduct(Σx, Σy)`](@ref) | no |
| [`vecmatmul(x, ΣA)`](@ref) | no |
