# API

```@meta
CurrentModule = SumSparseTensors
```

The SumSparseTensors.jl package provides a basic type

## Sum of sparse tensor types

```@docs
AbstractSumSparseTensor
AbstractSortedSumSparseTensor
sparsesum
Base.sort
```

## Accessing non-zero elements

```@docs
dimsorted
fiberptr
nnz
nzcoo
nzrange
nzval
Base.size
```

## Basic operations

```@docs
addaxes
Base.cat
collapse
mapcoo
mapcoo_sorted
resize
Base.sum
Base.vcat
```

## Linear Algebra Operations

```@docs
dot
dotstar
+
outer
tensorproduct
vecmatmul
```
