# SPDX-License-Identifier: CECILL-2.1

struct CustomUnitRange <: AbstractUnitRange{Int}
    start::Int
    stop::Int

    function CustomUnitRange(start, stop)
        stop = ifelse(start > stop, start - one(Int), stop)
        new(start, ifelse(start > stop, start - 1, stop))
    end
end

Base.length(r::CustomUnitRange) = r.stop - r.start + 1
Base.first(r::CustomUnitRange) = r.start
Base.last(r::CustomUnitRange) = r.stop

Base.iterate(r::CustomUnitRange) = r.start > r.stop ? nothing : (r.start, r.start)
function Base.iterate(r::CustomUnitRange, i)
    x = i + one(Int)
    x > r.stop ? nothing : (x, x)
end

Base.intersect(r1::CustomUnitRange, r2::CustomUnitRange) = CustomUnitRange(max(first(r1), first(r2)), min(last(r1), last(r2)))
Base.union(r1::CustomUnitRange, r2::CustomUnitRange) = CustomUnitRange(min(first(r1), first(r2)), max(last(r1), last(r2)))

function Base.getindex(r::CustomUnitRange, s::AbstractUnitRange)
    @boundscheck checkbounds(r, s)
    start = first(r) + first(s) - 1
    CustomUnitRange(first(r) + first(s) - 1, start + last(s) - 1)
end

Base.convert(::Type{CustomUnitRange}, r::AbstractUnitRange) = CustomUnitRange(first(r), last(r))
CustomUnitRange(r::AbstractUnitRange) = convert(CustomUnitRange, r)

