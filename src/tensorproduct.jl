# SPDX-License-Identifier: CECILL-2.1

#==============================================================================
Internal buffers for the tensor product.
==============================================================================#

struct TensorProductIndices{N,NA,NB} <: AbstractVector{NTuple{N,Int}}
    a::AbstractVector{NTuple{NA,Int}}
    b::AbstractVector{NTuple{NB,Int}}
end

struct TensorProductValues{T} <: AbstractVector{T}
    a::AbstractVector{T}
    b::AbstractVector{T}
end

function Base.getindex(x::TensorProductIndices, i)
    ai = (i - 1) ÷ size(x.b, 1) + 1
    bi = (i - 1) % size(x.b, 1) + 1
    (x.a[ai]..., x.b[bi]...)
end

function Base.getindex(x::TensorProductIndices, ij::Tuple)
    i, j = ij
    (x.a[i]..., x.b[j]...)
end

function Base.getindex(x::TensorProductValues, i)
    ai = (i - 1) ÷ size(x.b, 1) + 1
    bi = (i - 1) % size(x.b, 1) + 1
    x.a[ai] ⊗ x.b[bi]
end

function Base.getindex(x::TensorProductValues, ij::Tuple)
    i, j = ij
    x.a[i] ⊗ x.b[j]
end

Base.size(x::TensorProductIndices) = tuple(size(x.a, 1) * size(x.b, 1))
Base.size(x::TensorProductValues) = tuple(size(x.a, 1) * size(x.b, 1))


#==============================================================================
Tensor product structures (sorted and unsorted).
==============================================================================#

struct SumSparseTensorProduct{S,N} <: AbstractSumSparseTensor{S,N}
    ΣA::AbstractSumSparseTensor{S}
    ΣB::AbstractSumSparseTensor{S}
end

struct SortedSumSparseTensorProduct{S,N,I,J} <: AbstractSortedSumSparseTensor{S,N,I}
    ΣA::AbstractSortedSumSparseTensor{S}
    ΣB::AbstractSortedSumSparseTensor{S}
end

const AllSumTensorProduct{S,N} = Union{SumSparseTensorProduct{S,N},
                                       SortedSumSparseTensorProduct{S,N}} where {S,N}

function findnz(TP::AllSumTensorProduct{S,N}) where {S,N}
    Anzcoo, Anzval = findnz(TP.ΣA)
    Bnzcoo, Bnzval = findnz(TP.ΣB)
    NA, NB = length(size(TP.ΣA)), length(size(TP.ΣB))
    nzcoo = TensorProductIndices{NA+NB,NA,NB}(Anzcoo, Bnzcoo)
    nzval = TensorProductValues{S}(Anzval, Bnzval)
    nzcoo, nzval
end

nnz(Σx::AllSumTensorProduct) = nnz(Σx.ΣA) * nnz(Σx.ΣB)
nzinds(Σx::AllSumTensorProduct) = Iterators.product(nzinds(Σx.ΣA), nzinds(Σx.ΣB))
nzinds(Σx::SortedSumSparseTensorProduct{S,N,I}, slice) where {S,N,I} = Iterators.product(nzinds(Σx.ΣA, slice), nzinds(Σx.ΣB))
Base.axes(TP::AllSumTensorProduct) = (axes(TP.ΣA)..., axes(TP.ΣB)...)


"""
    tensorproduct(Σx, Σy)

Compute the tensor product between `Σx` and `Σy` such that if `Σx` is a
``N_x``-dimensional tensor and `Σy` is a ``N_y``-dimensional tensor the
resulting tensor will have ``N_x + N_y`` dimensions.
"""
tensorproduct

function tensorproduct(ΣA::AbstractSumSparseTensor{S,NA},
                       ΣB::AbstractSumSparseTensor{S,NB}) where {S,NA,NB}
    SumSparseTensorProduct{S,NA+NB}(ΣA, ΣB)
end

function tensorproduct(ΣA::AbstractSortedSumSparseTensor{S,NA,I},
                       ΣB::AbstractSortedSumSparseTensor{S,NB,J}) where {S,NA,NB,I,J}
    SortedSumSparseTensorProduct{S,NA+NB,I,J}(ΣA, ΣB)
end


#==============================================================================
Internal buffers for the contraction of a tensor product.

Note that the size of these buffers is not known so any function calling
`Base.size` will fail.
==============================================================================#

struct ContractIndices{N,TPI<:TensorProductIndices} <: AbstractVector{NTuple{N,Int}}
    inds::TPI
    ndims::NTuple{N,Int}
end

struct ContractValues{T} <: AbstractVector{T}
    values::TensorProductValues{T}
end

function Base.getindex(x::ContractIndices, ij::Tuple)
    tp_coo = x.inds[ij]
    ntuple(n -> tp_coo[x.ndims[n]], length(tp_coo) - 2)
end

Base.getindex(x::ContractValues, ij::Tuple) = x.values[ij]


#==============================================================================
Contraction of a (sorted) tensor product.
==============================================================================#

struct Contract{S,N,I} <: AbstractSortedSumSparseTensor{S,N,I}
    TP::SortedSumSparseTensorProduct{S}
    cdims::Tuple{Int,Int}
end

function Base.axes(C::Contract{S,N}) where {S,N}
    dims = (first(C.cdims), length(size(C.TP.ΣA)) + last(C.cdims))
    ndims = setdiff(ntuple(identity, length(axes(C.TP))), dims)
    ntuple(i -> axes(C.TP)[ndims[i]], N)
end

function findnz(C::Contract{S,N,I}) where {S,N,I}
    dims = (first(C.cdims), length(size(C.TP.ΣA)) + last(C.cdims))
    ndims = tuple(setdiff(ntuple(identity, N+2), dims)...)
    tpcoo, tpval = findnz(C.TP)
    ContractIndices(tpcoo, ndims), ContractValues(tpval)
end

struct ContractIdxIterator
    cdim
    inds
    TP

    ContractIdxIterator(C::Contract, slice) = new(
        first(C.cdims), nzinds(C.TP.ΣA, slice), C.TP
    )
end

function Base.iterate(it::ContractIdxIterator, state=missing)
    ΣA_nzcoo, _ = findnz(it.TP.ΣA)
    if ismissing(state)
        ret = iterate(it.inds)
    else
        ret = iterate(it.inds, state)
    end

    if ! isnothing(ret)
        ΣA_nzi, nextstate = ret
        ΣA_idxs = ΣA_nzcoo[ΣA_nzi]

        return Iterators.product(
            (ΣA_nzi,),
            nzinds(it.TP.ΣB, ΣA_idxs[it.cdim])
        ), nextstate
    end
end

nzinds(C::Contract, slice) = Iterators.flatten(ContractIdxIterator(C, slice))


"""
    contract(Σtp, i, j)

Compute the contraction of tensor product over the pair of dimensions `i` and
`j`. The tensor product must be sorted.
"""
contract

function contract(TP::SortedSumSparseTensorProduct{S,N,I,J}, a, b) where {S,N,I,J}
    NA = length(size(TP.ΣA))
    I == a && throw(ArgumentError("tensor product must NOT be sorted along the FIRST contracted dimension"))
    J ≠ b && throw(ArgumentError("tensor product must be sorted along the SECOND contracted dimension"))
    size(TP.ΣA, a) == size(TP.ΣB, b) || throw(DimensionMismatch("contracted dimensions should have the same size"))
    Contract{S,N-2,I}(TP, (a, b))
end

