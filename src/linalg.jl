# SPDX-License-Identifier: CECILL-2.1

"""
    dot(Σx, y)
    dot(y, Σx)

Compute the dot product between a dense tensor `y` and a sum of tensors `Σx`
of the same dimension of `y`.
"""
LinearAlgebra.dot

function LinearAlgebra.dot(x::AbstractArray{S,N}, Σy::AbstractSumSparseTensor{S,N}) where {S,N}
    out = zero(S)
    nzcoo, nzval = findnz(Σy)
    for nzi in nzinds(Σy)
        ii = nzcoo[nzi]
        v = nzval[nzi]
        out = out ⊕ (x[ii...] ⊗ v)
    end
    out
end

function LinearAlgebra.dot(Σx::AbstractSumSparseTensor{S,N}, y::AbstractArray{S,N}) where {S,N}
    out = zero(S)
    nzcoo, nzval = findnz(Σx)
    for nzi in nzinds(Σx)
        i, = nzcoo[nzi]
        v = nzval[nzi]
        out = out ⊕ (v ⊗ y[nzcoo[nzi]...])
    end
    out
end


"""
    dotstar(Σx, ΣA, Σy, idims, odims)

Compute the dot product
```math
\\sum_{i} \\sum_{j} \\mathbf{x}_i^\\top \\big( \\sum_{k} \\mathbf{A}_k \\big)^* \\mathbf{y}_j
```
where the ``^*`` operation is defined as
```math
\\mathbf{X}^* = \\mathbf{X}^0 + \\mathbf{X}^1 + \\mathbf{X}^2 + ...
```
The arguments `idims` and `odims` are the dimension for the multiply-sum
([`mulsum`](@ref)) operation.
"""
dotstar

function _has_changed(x, y)
    changed = false
    for i in eachindex(x)
        if ! (val(x[i]) ≈ val(y[i]))
            changed = true
            break
        end
    end
    changed
end

function dotstar(Σx::AbstractSumSparseTensor{S,N},
                 ΣA::AbstractSumSparseTensor{S},
                 Σy::AbstractSumSparseTensor{S,N},
                 idims,
                 odims) where {S,N}

    u_n = add(zeros(S, size(Σx)...), Σx)
    res = dot(u_n, Σy)
    prev_u_n = zeros(S, size(u_n)...)
    stop = false
    while ! stop
        u_n = mulsum(u_n, ΣA, idims, odims)
        res = res ⊕ dot(u_n, Σy)
        stop = isapprox(val.(prev_u_n), val.(u_n))
        prev_u_n = u_n
    end
    res
end

function add!(out::Array{S,N}, x::AbstractArray{S,N}, y::AbstractArray{S,N}) where {S,N}
    size(out) == size(x) == size(y) || throw(DimensionMismatch("$(size(out)) ≠ $(size(x)) ≠ $(size(y))"))
    copyto!(out, x)
    for i in eachindex(y)
        out[i] = out[i] ⊕ y[i]
    end
    out
end

function add!(out::Array{S,N}, x::AbstractArray{S,N}, Σy::AbstractSumSparseTensor{S,N}) where {S,N}
    size(out) == size(x) == size(Σy) || throw(DimensionMismatch("$(size(out)) ≠ $(size(x)) ≠ $(Σy.size)"))

    nzcoo, nzval = findnz(Σy)
    copyto!(out, x)
    for nzi in nzinds(Σy)
        ii = nzcoo[nzi]
        v = nzval[nzi]
        out[ii...] = out[ii...] ⊕ v
    end
    out
end

add(x::AbstractArray{S,N}, y::AbstractArray{S,N}) where {S,N} = add!(Array{S,N}(undef, size(x)...), x, y)
add(x::AbstractArray{S,N}, Σy::AbstractSumSparseTensor{S,N}) where {S,N} = add!(Array{S,N}(undef, size(x)...), x, Σy)
add(Σx::AbstractSumSparseTensor{S,N}, y::AbstractArray{S,N}) where {S,N} = add(y, Σx)

function add(Σx::AbstractSumSparseTensor{S,N}, Σy::AbstractSumSparseTensor{S,N}) where {S,N}
    size(Σx) ≠ size(Σy) && throw(DimensionMismatch("$(size(Σx)) ≠ $(size(Σy))"))
    xnzcoo, xnzval = findnz(Σx)
    ynzcoo, ynzval = findnz(Σy)
    SumSparseTensor(axes(Σx), vcat(xnzcoo, ynzcoo), vcat(xnzval, ynzval))
end


"""
    add(Σx, Σy)

Elementwise ``\\oplus``-addition between sum of tensors.
"""
add


function mul(x::AbstractArray{S,N},
             Σy::AbstractSumSparseTensor{S,M},
             dims::NTuple{N,Int}) where {S,N,M}

    nzcoo, nzval = findnz(Σy)
    newval = S[]
    newcoo = NTuple{M,Int}[]
    for nzi in nzinds(Σy)
        ii = nzcoo[nzi]
        coo = ntuple(n -> ii[dims[n]], N)
        v = nzval[nzi]
        if ! iszero(x[coo...])
            push!(newcoo, ii)
            push!(newval, x[coo...] ⊗ v)
        end
    end

    SumSparseTensor(axes(Σy), newcoo, newval)
end

function mul(x::AbstractArray{S,N},
             Σy::AbstractSumSparseTensor{S,M},
             z::AbstractArray{S,N},
             xdims::NTuple{N,Int},
             zdims::NTuple{N,Int}) where {S,N,M}

    nzcoo, nzval = findnz(Σy)
    newval = S[]
    newcoo = NTuple{M,Int}[]
    for nzi in nzinds(Σy)
        ii = nzcoo[nzi]
        xcoo = ntuple(n -> ii[xdims[n]], N)
        zcoo = ntuple(n -> ii[zdims[n]], N)
        v = nzval[nzi]
        if ! (iszero(x[xcoo...]) || iszero(z[zcoo...]))
            push!(newcoo, ii)
            push!(newval, x[xcoo...] ⊗ v ⊗ z[zcoo...])
        end
    end

    SumSparseTensor(axes(Σy), newcoo, newval)
end

function mul(x::AbstractArray{S,N},
             Σy::AbstractSortedSumSparseTensor{S,M,I},
             dims::NTuple{N,Int}) where {S,N,M,I}

    nzcoo, nzval = findnz(Σy)
    newval = S[]
    newcoo = NTuple{M,Int}[]
    counts = zeros(Int, size(Σy, I)+1)
    counts[1] = 1
    r = axes(Σy, I)
    for slice in r
        for nzi in nzinds(Σy, slice)
            ii = nzcoo[nzi]
            coo = ntuple(n -> ii[dims[n]], N)
            v = nzval[nzi]
            if ! iszero(x[coo...])
                push!(newcoo, ii)
                push!(newval, x[coo...] ⊗ v)

                i = ii[I] - first(r) + 1
                counts[i+1] += 1
            end
        end
    end
    ptr = cumsum(counts)

    SortedSumSparseTensor{S,M,I}(axes(Σy), ptr, newcoo, newval)
end

function mul(x::AbstractArray{S,N},
             Σy::AbstractSortedSumSparseTensor{S,M,I},
             z::AbstractArray{S,N},
             xdims::NTuple{N,Int},
             zdims::NTuple{N,Int}) where {S,N,M,I}

    nzcoo, nzval = findnz(Σy)
    newval = S[]
    newcoo = NTuple{M,Int}[]
    counts = zeros(Int, size(Σy, I)+1)
    counts[1] = 1
    r = axes(Σy, I)
    for slice in r
        for nzi in nzinds(Σy, slice)
            ii = nzcoo[nzi]
            xcoo = ntuple(n -> ii[xdims[n]], N)
            zcoo = ntuple(n -> ii[zdims[n]], N)
            v = nzval[nzi]
            if ! (iszero(x[xcoo...]) || iszero(z[zcoo...]))
                push!(newcoo, ii)
                push!(newval, x[xcoo...] ⊗ v ⊗ z[zcoo...])

                i = ii[I] - first(r) + 1
                counts[i+1] += 1
            end
        end
    end
    ptr = cumsum(counts)

    SortedSumSparseTensor{S,M,I}(axes(Σy), ptr, newcoo, newval)
end

"""
    mul(x, Σy, dims)
    mul(x, Σy, z, xdims, zdims)

Elementwise multiplication from the left (and from the right when `z` is
supplied) of a dense tensor and a sum of sparse tensors.
"""
mul


function _mulsum_check_dims(P, idims, odims)
    all(idims .<= P) || throw(ArgumentError("all elements of `idims = $idims` should be lower than `length(size(ΣA)) = $P`"))
    all(odims .<= P) || throw(ArgumentError("all elements of `odims = $odims` should be lower than `length(size(ΣA)) = $P`"))
    any(idims .∈ Ref(odims)) && throw(ArgumentError("`idims = $idims` and `odims = $odims` cannot overlap"))
end

function _unsorted_mulsum!(out::Array{S,N},
                           mulop,
                           X::AbstractArray{S,M},
                           ΣA::AbstractSumSparseTensor{S,P},
                           idims::NTuple{M},
                           odims::NTuple{N}) where {S,M,N,P}

    _mulsum_check_dims(P, idims, odims)

    fill!(out, zero(S))
    nzcoo, nzval = findnz(ΣA)
    for nzi in nzinds(ΣA)
        c, v = nzcoo[nzi], nzval[nzi]
        ii = ntuple(m -> c[idims[m]], M)
        jj = ntuple(n -> c[odims[n]], N)
        out[jj...] = out[jj...] ⊕ mulop(X[ii...], v)
    end
    out
end

function _sorted_mulsum!(out::Array{S,N},
                         mulop,
                         X::AbstractArray{S,M},
                         ΣA::AbstractSortedSumSparseTensor{S,P,I},
                         idims::NTuple{M},
                         odims::NTuple{N}) where {S,M,N,P,I}

    _mulsum_check_dims(P, idims, odims)

    fill!(out, zero(S))
    nzcoo, nzval = findnz(ΣA)
    for i in 1:size(X, I)
        for nzi in nzinds(ΣA, i)
            c, v = nzcoo[nzi], nzval[nzi]
            ii = ntuple(m -> c[idims[m]], M)
            jj = ntuple(n -> c[odims[n]], N)
            out[jj...] = out[jj...] ⊕ mulop(X[ii...], v)
        end
    end
    out
end

mulsum!(out, X, ΣA::AbstractSumSparseTensor, idims, odims) = _unsorted_mulsum!(out, (a, b) -> a ⊗ b, X, ΣA, idims, odims)
mulsum!(out, ΣA::AbstractSumSparseTensor, X, idims, odims) = _unsorted_mulsum!(out, (a, b) -> b ⊗ a, X, ΣA, idims, odims)

function mulsum!(out, X, ΣA::AbstractSortedSumSparseTensor{S,P,I}, idims, odims) where {S,P,I}
    if I <= length(size(X))
        _sorted_mulsum!(out, (a, b) -> a ⊗ b, X, ΣA, idims, odims)
    else
        _unsorted_mulsum!(out, (a, b) -> a ⊗ b, X, ΣA, idims, odims)
    end
end

function mulsum!(out, ΣA::AbstractSortedSumSparseTensor{S,P,I}, X, idims, odims) where {S,P,I}
    if I <= length(size(X))
        _sorted_mulsum!(out, (a, b) -> b ⊗ a, X, ΣA, idims, odims)
    else
        _unsorted_mulsum!(out, (a, b) -> b ⊗ a, X, ΣA, idims, odims)
    end
end

function mulsum(X::AbstractArray{S}, ΣA::AbstractSumSparseTensor{S}, idims, odims) where S
    osize = ntuple(n -> size(ΣA)[odims[n]], length(odims))
    mulsum!(Array{S,length(odims)}(undef, osize...), X, ΣA, idims, odims)
end

function mulsum(ΣA::AbstractSumSparseTensor{S}, X::AbstractArray{S}, idims, odims) where S
    osize = ntuple(n -> size(ΣA)[odims[n]], length(odims))
    mulsum!(Array{S,length(odims)}(undef, osize...), ΣA, X, idims, odims)
end

"""
    mulsum(X, ΣA, idims, odims)
    mulsum(ΣA, X, idims, odims)

Multiplies and sum the dimensions of `X` and `ΣA` along the dimensions
specified in `idims` and `odims`. `idims` is a tuple of indices such that
`idims[i]` is the ith dimension in `X` to multiply and sum along in `ΣA`.
`odims` is a tuple of indices indicating the dimensions in `ΣA` that are not
summed. For instance, if `X` is a 2-dimensional array and `ΣA` is a sum of
6-dimensional tensor then `mulsum(X, ΣA, (2, 4), (1, 3))` performs the
following computation:
```math
    y_{kl} = \\sum_{ijmn} x_{ij} \\otimes \\bar{A}_{kiljmn}
```
where ``\\bar{A}`` is the sum of tensors `ΣA`. The output is a tensor of size
`(size(ΣA)[odims[1]], size(ΣA)[odims[2], ...)`
Depending on the order of the arguments, the multiplication is from the left
or from the right.
"""
mulsum


"""
    mulsumstar(Σx, ΣA, idims, odims)

Similar to [`mulsum`](@ref) but taking the star operation on `ΣA` instead
of `ΣA` directly
"""
mulsumstar

function mulsumstar(Σx::AbstractSumSparseTensor{S,N},
                    ΣA::AbstractSumSparseTensor{S},
                    idims::NTuple{N,Int},
                    odims::NTuple{N,Int}) where {S,N}

    u_n = add(zeros(S, size(Σx)...), Σx)
    res = similar(u_n)
    prevres = similar(u_n)
    copyto!(res, u_n)
    copyto!(prevres, u_n)
    stop = false
    while ! stop
        u_n = mulsum(u_n, ΣA, idims, odims)
        res = add!(res, res, u_n)
        stop = ! _has_changed(res, prevres)
        copyto!(prevres, res)
    end
    res
end

# NOTE: this signature will match only if `ΣA` and `Σx` have different size.
function mulsumstar(ΣA::AbstractSumSparseTensor{S},
                    Σx::AbstractSumSparseTensor{S,N},
                    idims::NTuple{N,Int},
                    odims::NTuple{N,Int}) where {S,N}

    u_n = add(zeros(S, size(Σx)...), Σx)
    res = similar(u_n)
    prevres = similar(u_n)
    copyto!(res, u_n)
    copyto!(prevres, u_n)
    stop = false
    while ! stop
        u_n = mulsum(ΣA, u_n, idims, odims)
        res = add!(res, res, u_n)
        stop = ! _has_changed(res, prevres)
        copyto!(prevres, res)
    end
    res
end

