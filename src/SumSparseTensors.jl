# SPDX-License-Identifier: CECILL-2.1

module SumSparseTensors

using ChainRulesCore
using LinearAlgebra
using Semirings


include("customunitrange.jl")

# Types
export AbstractSumSparseTensor,
       AbstractSortedSumSparseTensor,
       SumSparseTensor,
       sliceptr,
       findnz,
       dimsorted,
       nnz,
       nzinds,
       sparsesum

include("sumsparsetensor.jl")


# Linear algebra operations
export add,
       contract,
       dot,
       dotstar,
       mul,
       mulsum,
       mulsumstar,
       tensorproduct

include("tensorproduct.jl")
include("linalg.jl")
include("rules.jl")


# Base operations
export addaxes,
       collapse,
       insertaxes,
       mapcoo,
       resize,
       updateaxes


include("base.jl")

end

