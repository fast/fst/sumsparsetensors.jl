# SPD-License-Identifier: CECILL-2.1

"""
    addaxes(Σx, n)

Create a new tensor with `n` additional dimension.
"""
addaxes

addaxes(Σx::AbstractSumSparseTensor, n::Integer) = addaxes(Σx, ntuple(i -> CustomUnitRange(1, 1), n)...)

function addaxes(Σx::AbstractSumSparseTensor, rs::AbstractUnitRange...)
    pad = ntuple(i -> 1, length(rs))
    nzcoo, nzval = findnz(Σx)
    newcoo = map(c -> (c..., pad...), nzcoo)
    SumSparseTensor((axes(Σx)..., rs...), newcoo, nzval)
end

function addaxes(Σx::AbstractSortedSumSparseTensor{S,N,I}, rs::AbstractUnitRange...) where {S,N,I}
    pad = ntuple(i -> 1, length(rs))
    nzcoo, nzval = findnz(Σx)
    newcoo = map(c -> (c..., pad...), nzcoo)
    SortedSumSparseTensor{S,N+length(rs),I}((axes(Σx)..., rs...), sliceptr(Σx), newcoo, nzval)
end


"""
    cat(Σx, Σy; dims)

Concatenate `Σx` and `Σy` over the dimensions `dims`.
"""
Base.cat

function Base.cat(Σx::AbstractSumSparseTensor{S,N},
                  Σy::AbstractSumSparseTensor{S,N};
                  dims = tuple(1:N...)) where {S,N}

    # Makes sure the input is a tuple.
    dims = tuple(dims...)

    ndims = setdiff(ntuple(identity, N), dims)
    for d in ndims
        axes(Σx, d) != axes(Σy, d) && throw(DimensionMismatch("axes do no match for dimensions $ndims"))
    end

    xnzcoo, xnzval = findnz(Σx)
    ynzcoo, ynzval = findnz(Σy)
    offset = ntuple(i -> i ∈ dims ? last(axes(Σx, i)) : 0, N)
    newcoo = vcat(xnzcoo, map(c -> c .+ offset, ynzcoo))
    newval = vcat(xnzval, ynzval)

    newaxes = ntuple(N) do i
        rx = axes(Σx, i)
        ry = axes(Σy, i)
        i ∈ dims ? CustomUnitRange(first(rx), last(rx) + length(ry)) : rx
    end

    SumSparseTensor(newaxes, newcoo, newval)
end

function Base.cat(Σx::AbstractSortedSumSparseTensor{S,N,I},
                  Σy::AbstractSortedSumSparseTensor{S,N,I};
                  dims) where {S,N,I}
    dims = tuple(dims...)

    ndims = setdiff(ntuple(identity, N), dims)
    for d in ndims
        axes(Σx, d) != axes(Σy, d) && throw(DimensionMismatch("axes do no match for dimensions $ndims"))
    end

    xnzcoo, xnzval = findnz(Σx)
    ynzcoo, ynzval = findnz(Σy)
    offset = ntuple(i -> i ∈ dims ? last(axes(Σx, i)) : 0, N)
    newcoo = vcat(xnzcoo, map(c -> c .+ offset, ynzcoo))
    newval = vcat(xnzval, ynzval)

    xptr, yptr = sliceptr(Σx), sliceptr(Σy)
    newptr = vcat(xptr[1:end-1], (xptr[end] - 1) .+ yptr)

    newaxes = ntuple(N) do i
        rx = axes(Σx, i)
        ry = axes(Σy, i)
        i ∈ dims ? CustomUnitRange(first(rx), last(rx) + last(ry)) : rx
    end
    SortedSumSparseTensor{S,N,I}(newaxes, newptr, newcoo, newval)
end


"""
    collapse(Σx)

Collapse the sum into a single term. The resulting sum of sparse tensor has at
most one non-zero element per possible coordinate.
"""
function collapse(Σx::AbstractSumSparseTensor{S,N}) where {S<:Semiring,N}
    nzcoo, nzval = findnz(Σx)
    coo_val = Dict{NTuple{N,Int},S}()
    for nzi in nzinds(Σx)
        k, v = nzcoo[nzi], nzval[nzi]
        coo_val[k] = get(coo_val, k, zero(S)) ⊕ v
    end

    newcoo, newval = NTuple{N,Int}[], S[]
    for (k, v) in coo_val
        push!(newcoo, k)
        push!(newval, v)
    end

    SumSparseTensor(axes(Σx), newcoo, newval)
end


"""
    copy(Σx)

Return a copy of the sum of sparse tensors. This will force the materialization
of the lazy operations such as [`tensorproduct`](@ref) or [`contract`](@ref).
"""
Base.copy

function Base.copy(Σx::AbstractSumSparseTensor{S,N}) where {S,N}
    coo = NTuple{N,Int}[]
    val = S[]

    nzcoo, nzval = findnz(Σx)
    for nzi in nzinds(Σx)
        push!(coo, nzcoo[nzi])
        push!(val, nzval[nzi])
    end

    SumSparseTensor(axes(Σx), coo, val)
end

function Base.copy(Σx::AbstractSortedSumSparseTensor{S,N,I}) where {S,N,I}
    coo = NTuple{N,Int}[]
    val = S[]

    nzcoo, nzval = findnz(Σx)
    counts = zeros(Int, size(Σx, I)+1)
    counts[1] = 1
    for i in axes(Σx, I)
        for nzi in nzinds(Σx, i)
            c = nzcoo[nzi]
            push!(coo, c)
            push!(val, nzval[nzi])

            ii = c[I] - first(axes(Σx, I)) + 1
            counts[ii+1] += 1
        end
    end
    ptr = cumsum(counts)

    SortedSumSparseTensor{S,N,I}(axes(Σx), ptr, coo, val)
end


"""
    insertaxes(Σx, newaxes...)

Create a new tensor with additional axes inserted as `newaxes[1]`, `newaxes[2]`,
etc.
"""
insertaxes

function insertaxes(Σx::AbstractSumSparseTensor{S,N}, extraaxes::Pair{Int,<:AbstractUnitRange}...) where {S,N}
    ! issorted(extraaxes, by = first) && throw(ArgumentError("inserted axes should be sorted in increasing order"))

    function insert(coo)
        n = 1
        m = 1
        ntuple(N + length(extraaxes)) do i
            if m > length(extraaxes) || n < first(extraaxes[m])
                ret = coo[n]
                n += 1
            else # n == first(extraaxes[m])
                ret = 1
                m += 1
            end
            ret
        end
    end
    nzcoo, nzval = findnz(Σx)
    newcoo = map(c -> insert(c), nzcoo)

    n = 1
    m = 1
    newaxes = ntuple(N + length(extraaxes)) do i
        if m > length(extraaxes) || n < first(extraaxes[m])
            ret = axes(Σx, n)
            n += 1
        else # n == first(extraaxes[m])
            r = last(extraaxes[m])
            ret = CustomUnitRange(first(r), last(r))
            m += 1
        end
        ret
    end

    SumSparseTensor(newaxes, newcoo, nzval)
end

function insertaxes(Σx::AbstractSortedSumSparseTensor{S,N,I}, extraaxes::Pair{Int,<:AbstractUnitRange}...) where {S,N,I}
    ! issorted(extraaxes, by = first) && throw(ArgumentError("inserted axes should be sorted in increasing order"))

    function insert(coo)
        n = 1
        m = 1
        ntuple(N + length(extraaxes)) do i
            if m > length(extraaxes) || n < first(extraaxes[m])
                ret = coo[n]
                n += 1
            else # n == first(extraaxes[m])
                ret = 1
                m += 1
            end
            ret
        end
    end
    nzcoo, nzval = findnz(Σx)
    newcoo = map(c -> insert(c), nzcoo)

    n = 1
    m = 1
    newaxes = ntuple(N + length(extraaxes)) do i
        if m > length(extraaxes) || n < first(extraaxes[m])
            ret = axes(Σx, n)
            n += 1
        else # n == first(extraaxes[m])
            r = last(extraaxes[m])
            ret = CustomUnitRange(first(r), last(r))
            m += 1
        end
        ret
    end

    newI = I
    for m in 1:length(extraaxes)
        if I >= first(extraaxes[m])
            newI += 1
        end
    end

    SortedSumSparseTensor{S,N+length(extraaxes),newI}(newaxes, sliceptr(Σx), newcoo, nzval)
end


"""
    map(f, Σx)

Apply `f(xᵢ)` for all non-zero elements `xᵢ` in `Σx`.
"""
Base.map

function Base.map(f, Σx::AbstractSumSparseTensor)
    nzcoo, nzval = findnz(Σx)
    SumSparseTensor(axes(Σx), nzcoo, map(f, nzval))
end

function Base.map(f, Σx::AbstractSortedSumSparseTensor{S,N,I}) where {S,N,I}
    nzcoo, nzval = findnz(Σx)
    newval = map(f, nzval)
    SortedSumSparseTensor{eltype(newval),N,I}(axes(Σx), sliceptr(Σx), nzcoo, newval)
end

function Base.map(f, TP::SumSparseTensorProduct{S,N}) where {S,N}
    ΣA = map(f, TP.ΣA)
    ΣB = map(f, TP.ΣB)
    newS = eltype(ΣA)
    SumSparseTensorProduct{newS,N}(ΣA, ΣB)
end

function Base.map(f, TP::SortedSumSparseTensorProduct{S,N,I,J}) where {S,N,I,J}
    ΣA = map(f, TP.ΣA)
    ΣB = map(f, TP.ΣB)
    newS = eltype(ΣA)
    SortedSumSparseTensorProduct{newS,N,I,J}(ΣA, ΣB)
end

function Base.map(f, C::Contract{S,N,I}) where {S,N,I}
    TP = (map(f, C.TP))
    newS = eltype(TP)
    Contract{newS,N,I}(TP, C.cdims)
end


"""
    mapcoo(f, Σx)

Apply `f(c[xᵢ])` for the coordinate `c[xᵢ]`associated to the non-zero elements
`xᵢ` in `Σx`.
"""
mapcoo

function mapcoo(f, Σx::AbstractSumSparseTensor)
    nzcoo, nzval = findnz(Σx)
    SumSparseTensor(axes(Σx), map(f, nzcoo),  nzval)
end


"""
    resize(Σx, newsize)

Return a new tensor of size `newsize` with the same non-zero elements of `Σx`.
"""
resize

_validate_size(size, newsize) = any(size .> newsize) && throw(ArgumentError("cannot resize with a smaller size"))

function resize(Σx::AbstractSumSparseTensor{S,N}, newsize) where {S,N}
    _validate_size(size(Σx), newsize)

    newaxes = ntuple(N) do i
        r = axes(Σx, i)
        CustomUnitRange(first(r), last(r) + (newsize[i] - length(r)))
    end
    SumSparseTensor(newaxes, findnz(Σx)...)
end

function resize(Σx::AbstractSortedSumSparseTensor{S,N,I}, newsize) where {S,N,I}
    _validate_size(size(Σx), newsize)

    newaxes = ntuple(N) do i
        r = axes(Σx, i)
        CustomUnitRange(first(r), last(r) + (newsize[i] - length(r)))
    end

    newptr = vcat(sliceptr(Σx), repeat(sliceptr(Σx)[end:end], newsize[I] - size(Σx)[I]))
    SortedSumSparseTensor{S,N,I}(newaxes, newptr, findnz(Σx)...)
end

resize(Σx::AbstractSumSparseTensor, newsize::Integer) = resize(Σx, tuple(newsize))


"""
    sum(Σx; dims)

Sum over the dimensions `dims`. Contrary to the `sum` of dense arrays,
dimension are dropped in the resulting sum of sparse tensors.
"""
Base.sum

function Base.sum(Σx::AbstractSumSparseTensor{S,N}; dims) where {S,N}
    dims = tuple(dims...)
    ndims = tuple(setdiff(ntuple(identity, N), dims)...)
    M = length(ndims)

    nzcoo, nzval = findnz(Σx)
    newcoo = Array{NTuple{M,Int}}(undef, length(nzcoo))
    for (i, nzi) in enumerate(nzinds(Σx))
        c = nzcoo[nzi]
        newcoo[i] = ntuple(i -> c[ndims[i]], M)
    end

    newaxes = ntuple(M) do i
        axes(Σx)[ndims[i]]
    end

    SumSparseTensor(newaxes, newcoo, nzval)
end


"""
    updateaxes(Σx, newaxes)

Return a tensor `Σy` with the same non-zero elements of `Σx` and
    axes(Σy, i) = min(axes(Σx, i), newaxes[i]):max(axes(Σx, i), newaxes[i])
"""
updateaxes

function updateaxes(Σx::AbstractSumSparseTensor{S,N}, newaxes::NTuple{N,<:AbstractUnitRange}) where {S,N}
    newaxes = ntuple(N) do i
        union(axes(Σx, i), CustomUnitRange(newaxes[i]))
    end
    SumSparseTensor(newaxes, findnz(Σx)...)
end

function updateaxes(Σx::AbstractSortedSumSparseTensor{S,N,I}, newaxes::NTuple{N,<:AbstractUnitRange}) where {S,N,I}
    newaxes = ntuple(N) do i
        union(axes(Σx, i), CustomUnitRange(newaxes[i]))
    end

    ptr = sliceptr(Σx)
    newptr = vcat(
        repeat(ptr[1:1], first(axes(Σx, I)) - first(newaxes[I])),
        sliceptr(Σx),
        repeat(ptr[end:end], last(newaxes[I]) - last(axes(Σx, I)))
    )
    SortedSumSparseTensor{S,N,I}(newaxes, newptr, findnz(Σx)...)
end

updateaxes(Σx::AbstractSumSparseTensor, newaxes::AbstractUnitRange) = updateaxes(Σx, tuple(newaxes))

