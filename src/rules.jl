# SPDX-License-Identifier: CECILL-2.1

function ChainRulesCore.rrule(
    ::typeof(dot),
    x::Array{S,N},
    Σy::AbstractSumSparseTensor{S,N}
) where {S<:Semiring,N}
    out = dot(x, Σy)

    function dot_pullback(z̄)
        nzcoo, nzval = findnz(Σy)
        T = valtype(S)
        tT = Tangent{S}
        x̄ = fill!(similar(x, tT, axes(x)), Tangent{S}(val = T(0)))
        V̄ = similar(nzval, tT)

        for nzi in nzinds(Σy)
            ii = nzcoo[nzi]
            v = nzval[nzi]
            x̄i = ∂sum(out, x[ii...] ⊗ v) * ∂rmul(x[ii...], v) * z̄.val # TODO: fail on ExpSem
            x̄[ii...] += Tangent{S}(val = x̄i)
            V̄i = ∂sum(out, x[ii...] ⊗ v) * ∂lmul(x[ii...], v) * z̄.val
            V̄[nzi] = Tangent{S}(val = V̄i)
        end

        NoTangent(), x̄, Tangent{typeof(Σy)}(nzval = V̄)
    end

    out, dot_pullback
end

function ChainRulesCore.rrule(
    ::typeof(dot),
    Σy::AbstractSumSparseTensor{S,N},
    x::Array{S,N},
) where {S<:Semiring,N}
    out = dot(Σy, x)

    function dot_pullback(z̄)
        nzcoo, nzval = findnz(Σy)
        T = valtype(S)
        tT = Tangent{S}
        x̄ = fill!(similar(x, tT, axes(x)), Tangent{S}(val = T(0)))
        V̄ = similar(nzval, tT)

        for nzi in nzinds(Σy)
            ii = nzcoo[nzi]
            v = nzval[nzi]
            x̄i = ∂sum(out, v ⊗ x[ii...]) * ∂lmul(v, x[ii...]) * z̄.val # TODO: fail on ExpSem
            x̄[ii...] += Tangent{S}(val = x̄i)
            V̄i = ∂sum(out, v ⊗ x[ii...]) * ∂rmul(v, x[ii...]) * z̄.val
            V̄[nzi] = Tangent{S}(val = V̄i)
        end

        NoTangent(), Tangent{typeof(Σy)}(nzval = V̄), x̄
    end

    out, dot_pullback
end

function ChainRulesCore.rrule(
    ::typeof(add!),
    out::Array{S,N},
    x::AbstractArray{S,N},
    Σy::AbstractSumSparseTensor{S,N}
) where {S<:Semiring,N}
    add!(out, x, Σy)

    function add_pullback(z̄)
        nzcoo, nzval = findnz(Σy)

        T = valtype(S) # e.g. Float64
        tT = Tangent{S}
        x̄ = fill!(similar(x, tT, axes(x)), Tangent{S}(val = T(0)))
        V̄ = similar(nzval, tT)

        for nzi in nzinds(Σy)
            ii = nzcoo[nzi]
            v = nzval[nzi]
            x̄i = ∂sum(out[ii...], x[ii...]) * z̄[ii...].val
            x̄[ii...] += Tangent{S}(val = x̄i) #TODO: xi might be ::ZeroTangent
            V̄i = ∂sum(out[ii...], v) * z̄[ii...].val
            V̄[nzi] = Tangent{S}(val = V̄i) #TODO: same here
        end
        NoTangent(), ZeroTangent(), x̄, Tangent{typeof(Σy)}(nzval = V̄)
    end
    out, add_pullback
end

function ChainRulesCore.rrule(
    ::typeof(mulsum),
    X::AbstractArray{S,M},
    ΣA::AbstractSortedSumSparseTensor{S,P,I},
    idims::NTuple{M},
    odims::NTuple{N},
) where {S<:Semiring,M,N,P,I}
    out = mulsum(X, ΣA, idims, odims)
    length(size(X)) <= I || throw(ArgumentError("Pullback for unsorted mulsum is not implemented!"))

    function mulsum_pullback(z̄)
        nzcoo, nzval = findnz(ΣA)
        T = valtype(S) # e.g. Float64
        tT = Tangent{S}
        X̄ = fill!(similar(X, tT, axes(X)), Tangent{S}(val = T(0)))
        V̄ = similar(nzval, tT)

        # TODO: nzinds will change with new API
        for i in 1:size(X, I)
            for nzi in nzinds(ΣA, i)
                c, v = nzcoo[nzi], nzval[nzi]
                ii = ntuple(m -> c[idims[m]], M)
                jj = ntuple(n -> c[odims[n]], N)

                x̄i = ∂sum(out[jj...], X[ii...] ⊗ v) * ∂rmul(X[ii...], v) * z̄[jj...].val
                v̄  = ∂sum(out[jj...], X[ii...] ⊗ v) * ∂lmul(X[ii...], v) * z̄[jj...].val

                X̄[i]  += Tangent{S}(val = T(x̄i))
                V̄[nzi] = Tangent{S}(val = T(v̄))
            end
        end
        NoTangent(), X̄, Tangent{typeof(ΣA)}(nzval = V̄), NoTangent(), NoTangent()
    end

    return out, mulsum_pullback
end

function ChainRulesCore.rrule(
    ::typeof(mulsum),
    ΣA::AbstractSortedSumSparseTensor{S,P,I},
    X::AbstractArray{S,M},
    idims::NTuple{M},
    odims::NTuple{N},
) where {S<:Semiring,M,N,P,I}
    out = mulsum(ΣA, X, idims, odims)
    length(size(X)) <= I || throw(ArgumentError("Pullback for unsorted mulsum is not implemented!"))

    function mulsum_pullback(z̄)
        nzcoo, nzval = findnz(ΣA)
        T = valtype(S) # e.g. Float64
        tT = Tangent{S}
        X̄ = fill!(similar(X, tT, axes(X)), Tangent{S}(val = T(0)))
        V̄ = similar(nzval, tT)

        # TODO: nzinds will change with new API
        for i in 1:size(X, I)
            for nzi in nzinds(ΣA, i)
                c, v = nzcoo[nzi], nzval[nzi]
                ii = ntuple(m -> c[idims[m]], M)
                jj = ntuple(n -> c[odims[n]], N)

                x̄i = ∂sum(out[jj...], v ⊗ X[ii...]) * ∂lmul(v, X[ii...]) * z̄[jj...].val
                v̄  = ∂sum(out[jj...], v ⊗ X[ii...]) * ∂rmul(v, X[ii...]) * z̄[jj...].val

                X̄[i]  += Tangent{S}(val = T(x̄i))
                V̄[nzi] = Tangent{S}(val = T(v̄))
            end
        end
        NoTangent(), Tangent{typeof(ΣA)}(nzval = V̄), X̄, NoTangent(), NoTangent()
    end

    return out, mulsum_pullback
end
