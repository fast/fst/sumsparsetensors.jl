# SPDX-License-Identifier: CECILL-2.1

"""
    abstract type AbstractSumSparseTensor{S,N} end

Base type representing a sum of sparse tensors. `S` is the element type of the
tensor and `N` is the number of dimension of the tensor.
"""
abstract type AbstractSumSparseTensor{S,N} end


"""
    abstract type AbstractSumSparseTensor{S,N,I} end

Base type representing sum of sparse tensors with non-zero elements sorted
across the `I`th dimension. See also [`AbstractSumSparseTensor`](@ref).
"""
abstract type AbstractSortedSumSparseTensor{S,N,I} <: AbstractSumSparseTensor{S,N} end


"""
    Base.axes(Σx[, dim])

Return the tuple of valid indices for each axis in element of `Σx`.
"""
Base.axes
Base.axes(Σx::AbstractSumSparseTensor{S,N}, d) where {S,N} = d ≤ N ? axes(Σx)[d] : Base.OneTo(1)


"""
    dimsorted(sorted_Σx)

Return the dimension along which the coordinate of the non-zero elements
of `Σx` are sorted.
"""
dimsorted(::AbstractSortedSumSparseTensor{S,N,I}) where {S,N,I} = I


"""
    eltype(Σx)

Return the element's type of the terms of the sum of tensors.
"""
Base.eltype(Σx::AbstractSumSparseTensor{S}) where S = S


"""
    sliceptr(sorted_Σx)

Return an array `ptr` such that the range of non-zero indices of the ``i``th
slice is given by `ptr[i]:ptr[i+1]-1`.
"""
sliceptr


"""
    findnz(Σx)

Return the coordinates and the values of the non-zero elements.
"""
findnz


"""
    nnz(Σx)

Return the number of non-zero elements in the sum of sparse tensor `Σx`.
"""
nnz(Σx::AbstractSumSparseTensor)


"""
    nzinds(Σx)

Return an iterable over the indices of the non-zero elements.
"""
nzinds


"""
    Base.size(Σx[, dim])

Return the size of the tensor resulting from the sum `Σx`.
"""
Base.size(Σx::AbstractSumSparseTensor) = map(length, axes(Σx))
Base.size(Σx::AbstractSumSparseTensor{S,N}, d) where {S,N} = d ≤ N ? size(Σx)[d] : 1


#==============================================================================
Concrete types
==============================================================================#

struct SumSparseTensor{S,N,M} <: AbstractSumSparseTensor{S,N}
    axes::NTuple{N,CustomUnitRange}
    nzcoo::AbstractArray{NTuple{N,Int},M}
    nzval::AbstractArray{S,M}
end

findnz(Σx::SumSparseTensor) = Σx.nzcoo, Σx.nzval
nnz(Σx::SumSparseTensor) = length(Σx.nzcoo)
nzinds(Σx::SumSparseTensor) = 1:nnz(Σx)
Base.axes(Σx::SumSparseTensor) = Σx.axes


struct SortedSumSparseTensor{S,N,I,M} <: AbstractSortedSumSparseTensor{S,N,I}
    axes::NTuple{N,CustomUnitRange}
    ptr::AbstractVector{Int}
    nzcoo::AbstractArray{NTuple{N,Int},M}
    nzval::AbstractArray{S,M}

    function SortedSumSparseTensor{S,N,I}(axes, ptr, nzcoo, nzval) where {S,N,I}
        size(nzcoo) ≠ size(nzval) && throw(DimensionMistmatch("size(nzcoo) ≠ size(nzval)"))
        M = length(size(nzcoo))
        new{S,N,I,M}(axes, ptr, nzcoo, nzval)
    end
end

sliceptr(Σx::SortedSumSparseTensor) = Σx.ptr
findnz(Σx::SortedSumSparseTensor) = Σx.nzcoo, Σx.nzval
nnz(Σx::SortedSumSparseTensor) = length(Σx.nzcoo)
nzinds(Σx::SortedSumSparseTensor) = 1:nnz(Σx)

function nzinds(Σx::SortedSumSparseTensor{S,N,I}, slice) where {S,N,I}
    r = axes(Σx, I)
    i = slice - first(r) + 1
    Σx.ptr[i]:Σx.ptr[i+1]-1
end

Base.axes(Σx::SortedSumSparseTensor) = Σx.axes


"""
    sparsesum(nzcoo, nzval, [; axes])

Create a sum of sparse tensor from the coordinates of the non-zero elements
`nzcoo` and their corresponding value `nzval`. If `axes` is not provided it is
determined from minimum and maximum values in `nzcoo`.
"""
sparsesum

function sparsesum(nzcoo::AbstractVector{NTuple{N,Int}}, nzval::AbstractVector; axes = missing) where N
    length(nzcoo) == length(nzval) || throw(ArgumentError("`nzcoo` and `nzval` should have the same length"))

    axes = if ismissing(axes)
        ntuple(N) do n
            start = min(1, mapreduce(c -> c[n], min, nzcoo))
            stop = mapreduce(c -> c[n], max, nzcoo)
            CustomUnitRange(start, stop)
        end
    else
        map(CustomUnitRange, axes)
    end

    SumSparseTensor(axes, copy(nzcoo), copy(nzval))
end

function sparsesum(nzcoo::AbstractVector{Int}, nzval::AbstractVector; axis = missing)
    axis = if ismissing(axis)
        CustomUnitRange(min(1, minimum(nzcoo)), maximum(nzcoo))
    else
        CustomUnitRange(axis)
    end
    sparsesum(tuple.(nzcoo), nzval; axes = (axis,))
end


"""
    sort(Σx, dim)

Returns an sum of sparse tensors such that the non-zero elements are sorted
in the dimension `dim` of the tensor.
"""
function Base.sort(Σx::AbstractSumSparseTensor{S,N}, dim) where {S,N}
    nzcoo, nzval = findnz(Σx)

    perm = sortperm(nzcoo, by = coo -> coo[dim])
    newcoo = nzcoo[perm]
    newval = nzval[perm]

    counts = zeros(Int, size(Σx, dim)+1)
    counts[1] = 1
    r = axes(Σx, dim)
    for nzi in nzinds(Σx)
        coo = nzcoo[nzi]
        i = coo[dim] - first(r) + 1
        counts[i+1] += 1
    end
    ptr = cumsum(counts)

    SortedSumSparseTensor{S,N,dim}(axes(Σx), ptr, newcoo, newval)
end

