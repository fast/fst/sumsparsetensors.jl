# SumSparseTensors.jl

This package is semiring linear algebra core of the
[TensorFSTs.jl](https://gitlab.lisn.upsaclay.fr/fast/TensorFSTs.jl)
package.

For most of the users, this package should not be used directly.

